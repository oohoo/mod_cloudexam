YUI.add('moodle-mod_cloudexam-util-base', function (Y, NAME) {

/**
 * The Moodle.mod_cloudexam.util classes provide cloudexam-related utility functions.
 *
 * @module moodle-mod_cloudexam-util
 * @main
 */

Y.namespace('Moodle.mod_cloudexam.util');

/**
 * A collection of general utility functions for use in cloudexam.
 *
 * @class Moodle.mod_cloudexam.util
 * @static
 */


}, '@VERSION@');

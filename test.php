<?php

require_once(__DIR__ . '/../../config.php');
global $CFG;

//require_once('config.php');
//require_once('locallib.php');
//
//$attemptobj = quiz_create_attempt_handling_errors(4, 3);
//print_object($attemptobj);die;
//print_object(json_encode('*******BREAK*******'));
//const url = 'https://panoramaforbusiness.oohoo.biz/webservice/rest/server.php';
const url = 'http://kais.biz/edunao/webservice/rest/server.php';
const wstoken = 'ecea25f6e4cb1e259d2843e140a47dd5';

//const json = '{
//    "userid" : 3, 
//    "cloudexamid" : 32,
//    "data" : {
//        "questiondata" : {"_order":"23,21,22"}, 
//        "formdata" : {"q4:1_:flagged":"0","q4:1_:sequencecheck":"1","q4:1_answer":"0"}, 
//        "questionid" : 9
//    }
//}';

function get_quiz_access_information() {
    $function = 'get_quiz_access_information';
    $params = [
//        'wstoken' => '23969056997b153b5f3766825f55dfcf',
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5', // With the proper persmissions to call the mod_quiz functions 
//        'wstoken' => '7a4424d9adf4de775c5b3b5ecf5851fc2',
        'moodlewsrestformat' => 'json',
        'wsfunction' => 'mod_quiz_' . $function,
        'quizid' => 1
    ];
    $param_string = http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//    curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
//    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    print_object('***********get_quiz_access_information*********');
//{"canattempt":true,"canmanage":true,"canpreview":true,"canreviewmyattempts":true,"canviewreports":true,"accessrules":[],"activerulenames":["quizaccess_openclosedate"],"preventaccessreasons":[],"warnings":[]}
    $output = curl_exec($ch);
//    print_object($output);
    curl_close($ch);
}

function get_user_attempts($userid) {
    $function = 'get_user_attempts';
//    print_object('$output');
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'wsfunction' => 'mod_quiz_' . $function,
        'moodlewsrestformat' => 'json',
        'quizid' => 1,
        'userid' => $userid,
        'status' => "all",
        'includepreviews' => true
    ];
    $param_string = http_build_query($params, '', '&');
    print_object('***********get_user_attempts*********');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    curl_setopt($ch, CURLOPT_URL, url);

    $output = curl_exec($ch);
    print_object($output);
    curl_close($ch);
}

function get_quizzes_by_courses() {
    $function = 'get_quizzes_by_courses';
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'moodlewsrestformat' => 'json',
        'wsfunction' => 'mod_quiz_' . $function,
        'courseids' => array(2)
    ];
    $param_string = http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    print_object('***********get_quizzes_by_courses*********');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    print_object($output);
    curl_close($ch);
}

function start_attempt() {
    $function = 'start_attempt_modified';
//    print_object('$output');
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'wsfunction' => 'mod_quiz_' . $function,
        'moodlewsrestformat' => 'json',
        'quizid' => 1,
        'userid' => 4,
    ];
    $param_string = http_build_query($params, '', '&');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    print_object('***********start_attempt*********');
    print_object($output);
    $decoded = json_decode($output);
//    $attemptid = $decoded->attempt->id;
//    $page = $decoded->attempt->currentpage;
    print_object('********start_attempt****** DECODED ');
    print_object($decoded);
//    print_object($attemptid);
//    print_object($page);
    curl_close($ch);
}

function get_attempt_data($attemptid, $page) {
    $function = 'get_attempt_data';
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'moodlewsrestformat' => 'json',
        'wsfunction' => 'mod_quiz_' . $function,
        'attemptid' => $attemptid,
        'page' => $page,
        'preflightdata' => [
            ['name' => 'confirmdatasaved', 'value' => 1] //This is hard coded. Need to find out how to read the preflight data
        ],
    ];
    $param_string = http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    print_object('***********get_attempt_data*********');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    $decoded = json_decode($output);
    print_object($decoded);
    $squencecheck = $decoded->questions;
    $seqCheck = [];
    foreach ($squencecheck as $seq) {
        array_push($seqCheck, $seq->sequencecheck);
    }

    print_object($squencecheck);
    print_object($output);
    curl_close($ch);
}

function view_attempt($attemptid, $page) {
    $function = 'view_attempt';
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'moodlewsrestformat' => 'json',
        'wsfunction' => 'mod_quiz_' . $function,
        'attemptid' => $attemptid,
        'page' => $page,
        'preflightdata' => [
            ['name' => 'confirmdatasaved', 'value' => 1] //This is hard coded. Need to find out how to read the preflight data
        ],
    ];
    $param_string = http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    print_object('***********view_attempt*********');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    print_object($output);
    curl_close($ch);
}

function process_attempt($attemptid) {
    $function = 'process_attempt';
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'moodlewsrestformat' => 'json',
        'wsfunction' => 'mod_quiz_' . $function,
        'attemptid' => $attemptid,
        'data' => [
//            ['name' => 'q5:5_:sequencecheck', 'value' => 1], //This is hard coded. Need to find out how to read the preflight data
//            
//            
//            ['name' => 'q5:5_answer', 'value' => 1] //This is hard coded. Need to find out how to read the preflight data
        ],
        'finishattempt' => true,
        'timeup' => false,
        'preflightdata' => [
//            ['name' => 'confirmdatasaved', 'value' => 1] //This is hard coded. Need to find out how to read the preflight data
        ],
    ];
    $param_string = http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    print_object('***********process_attempt*********');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    print_object($output);
    curl_close($ch);
}

function view_attempt_review($attemptid) {
    $function = 'view_attempt_review';
    $params = [
        'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
        'moodlewsrestformat' => 'json',
        'wsfunction' => 'mod_quiz_' . $function,
        'attemptid' => $attemptid,
    ];
    $param_string = http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
    print_object('***********view_attempt_review*********');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    print_object($output);
    curl_close($ch);
}

function get_answers_order($questionid, $quizattempt) {
    global $DB;
    $questionattemptid = $DB->get_record('question_attempts', ['questionid' => $questionid, 'questionusageid' => $quizattempt]); //1
    $questionattemptstepid = $DB->get_record('question_attempt_steps', ['questionattemptid' => $questionattemptid->id, 'state' => 'todo']); //2
    $order = $DB->get_record('question_attempt_step_data', ['attemptstepid' => $questionattemptstepid->id, 'name' => '_order']);
//        print_object($order);

    $str_order = explode(',', $order->value);
    print_object($str_order);
}

global $DB, $CFG;
//$data = [
//            ['name' => 'q5:5_:sequencecheck', 'value' => 1], //This is hard coded. Need to find out how to read the preflight data
//            
//            
//            ['name' => 'q5:5_answer', 'value' => 1] //This is hard coded. Need to find out how to read the preflight data
//        ];
//  $params = [
//            'wstoken' => 'ecea25f6e4cb1e259d2843e140a47dd5',
//            'moodlewsrestformat' => 'json',
//            'wsfunction' => 'mod_cloudexam_' . 'view_attempt_review',
//            'attemptid' => 13,
//            'data' => $data,
//            'finishattempt' => true,
//            'timeup' => false,
//            'preflightdata' => [
//                 false,
//            ],
//        ];
// $dir = $CFG->dataroot . '/cloudexam/';
//// print_object($logpath);die;
//            if (!file_exists($dir)) {
//                $old = umask(0);
//                mkdir($dir, 0777);
//                umask($old);
//               
//            }
//             $logpath = $dir . 'log.txt';
//          
//                file_put_contents($logpath, date("Y-m-d H:i:s", time()) . ' :' . "\r\n", FILE_APPEND);
//                file_put_contents($logpath, json_encode($params) . "\r\n", FILE_APPEND);
//    file_put_contents($logpath, json_encode($ex) . "\r\n", FILE_APPEND);
//                
//         


//get_answers_order(2,6);
//get_quiz_access_information();
 
 
// CONTEXT_SYSTEM::
$js = '{"attempt":{"id":23,"quiz":50,"userid":4,"attempt":4,"uniqueid":23,"layout":"1,0,2,0,3,0,4,0,5,0","currentpage":0,"preview":0,"state":"inprogress","timestart":1613024421,"timefinish":0,"timemodified":1613024421,"timemodifiedoffline":1613024421,"timecheckstate":null,"sumgrades":null},"warnings":[]}';
// print_object(json_decode($js));die;
$ws_call = new \mod_cloudexam\process_eep_json_1();
//$ws_call->process_engine();
//$ws_call = new \mod_cloudexam\process_eep_json(url, wstoken);
//$ws_call->get_courses_with_cloudexam();
//$ws_call->
//print_object($ws_call->getEep_order());
//        should be replaced with cloudexam
//$number_of_questions = $DB->count_records('quiz_slots', ['quizid' => 1]);
//print_object($number_of_questions);
//get_user_attempts(4);
//get_quizzes_by_courses();
//start_attempt();
//for ($i =1; $i<6; $i++) {
//    get_attempt_data(1, 0);
//view_attempt(6, 2);
//    process_attempt(6);
//}
//get_attempt_data(6, 0);
//view_attempt(14, 0);
//get_attempt_data(14, 0);
//process_attempt(14);
//
//Post Process

//
//get_user_attempts();
//get_quiz_access_information();
//get_quizzes_by_courses();
//get_user_attempts(4);
//view_attempt_review(6);
//
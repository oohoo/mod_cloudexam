<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This page is the entry page into the cloudexam UI. Displays information about the
 * cloudexam to students and teachers, and lets students see their previous attempts.
 *
 * @package   mod_cloudexam
 * @based on  original work with copyright: 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @copyright 2019 onwards Edunao SA
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_cloudexam\cloudexam_upload;

require_once(__DIR__ . '/../../config.php');
require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->dirroot . '/mod/cloudexam/locallib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/course/format/lib.php');
require_once($CFG->dirroot . '/mod/cloudexam/lib/cloudexam_lib.php');
require_once($CFG->dirroot . '/mod/cloudexam/classes/cloudexam_upload.class.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID, or ...
$q  = optional_param('q', 0, PARAM_INT);  // CloudExam ID.

if ($id) {
    if (!$cm = get_coursemodule_from_id('cloudexam', $id)) {
        print_error('invalidcoursemodule');
    }
//    print_object($cm);
    if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('coursemisconf');
    }
} else {
    if (!$cloudexam = $DB->get_record('cloudexam', array('id' => $q))) {
        print_error('invalidcloudexamid', 'cloudexam');
    }
    if (!$course = $DB->get_record('course', array('id' => $cloudexam->course))) {
        print_error('invalidcourseid');
    }
    if (!$cm = get_coursemodule_from_instance("cloudexam", $cloudexam->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
}

// Check login and get context.
require_login($course, false, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/cloudexam:view', $context);

// Cache some other capabilities we use several times.
$canattempt    = has_capability('mod/cloudexam:attempt', $context);
$canreviewmine = has_capability('mod/cloudexam:reviewmyattempts', $context);
$canpreview    = has_capability('mod/cloudexam:preview', $context);

// Create an object to manage all the other (non-roles) access rules.
$timenow       = time();
$cloudexamobj  = cloudexam::create($cm->instance, $USER->id);
$accessmanager = new cloudexam_access_manager($cloudexamobj, $timenow,
    has_capability('mod/cloudexam:ignoretimelimits', $context, null, false));
$cloudexam     = $cloudexamobj->get_cloudexam();

/** EDUNAO PATCH  Redirects Student to the waiting page **/
if (!has_capability('mod/cloudexam:manage', $context) && !\mod_cloudexam\is_open($cm->instance)) {
    redirect($CFG->wwwroot . '/mod/cloudexam/pages/waiting_view.php?cloudexamid=' . $cm->instance);
}
/** END EDUNAO PATCH **/

/**EDUNAO PATCH**/
$uploadresult = false;
if ($action = optional_param('action', 0, PARAM_TEXT)) {

    switch ($action){
        case 'upload_exam':
            $cloudexam_upload = new mod_cloudexam\cloudexam_upload($cm->id);
            $uploadresult     = $cloudexam_upload->eep_upload_cloudexam($cm->id);
            break;
        case 'finish_exam':
            \mod_cloudexam\close_exam($cm->id);
            break;
    }

}

/**END EDUNAO PATCH**/


// Trigger course_module_viewed event and completion.
cloudexam_view($cloudexam, $course, $cm, $context);

// Initialize $PAGE, compute blocks.
$PAGE->set_url('/mod/cloudexam/view.php', array('id' => $cm->id));

// Create view object which collects all the information the renderer will need.
$viewobj                = new mod_cloudexam_view_object();
$viewobj->accessmanager = $accessmanager;
$viewobj->canreviewmine = $canreviewmine || $canpreview;

// Get this user's attempts.
$attempts            = cloudexam_get_user_attempts($cloudexam->id, $USER->id, 'finished', true);
$lastfinishedattempt = end($attempts);
$unfinished          = false;
$unfinishedattemptid = null;
if ($unfinishedattempt = cloudexam_get_user_attempt_unfinished($cloudexam->id, $USER->id)) {
    $attempts[] = $unfinishedattempt;

    // If the attempt is now overdue, deal with that - and pass isonline = false.
    // We want the student notified in this case.
    $cloudexamobj->create_attempt_object($unfinishedattempt)->handle_if_time_expired(time(), false);

    $unfinished = $unfinishedattempt->state == cloudexam_attempt::IN_PROGRESS ||
                  $unfinishedattempt->state == cloudexam_attempt::OVERDUE;
    if (!$unfinished) {
        $lastfinishedattempt = $unfinishedattempt;
    }
    $unfinishedattemptid = $unfinishedattempt->id;
    $unfinishedattempt   = null; // To make it clear we do not use this again.
}
$numattempts = count($attempts);

$viewobj->attempts    = $attempts;
$viewobj->attemptobjs = array();
foreach ($attempts as $attempt) {
    $viewobj->attemptobjs[] = new cloudexam_attempt($attempt, $cloudexam, $cm, $course, false);
}

// Work out the final grade, checking whether it was overridden in the gradebook.
if (!$canpreview) {
    $mygrade = cloudexam_get_best_grade($cloudexam, $USER->id);
} else if ($lastfinishedattempt) {
    // Users who can preview the cloudexam don't get a proper grade, so work out a
    // plausible value to display instead, so the page looks right.
    $mygrade = cloudexam_rescale_grade($lastfinishedattempt->sumgrades, $cloudexam, false);
} else {
    $mygrade = null;
}

$mygradeoverridden = false;
$gradebookfeedback = '';

$grading_info = grade_get_grades($course->id, 'mod', 'cloudexam', $cloudexam->id, $USER->id);
if (!empty($grading_info->items)) {
    $item = $grading_info->items[0];
    if (isset($item->grades[$USER->id])) {
        $grade = $item->grades[$USER->id];

        if ($grade->overridden) {
            $mygrade           = $grade->grade + 0; // Convert to number.
            $mygradeoverridden = true;
        }
        if (!empty($grade->str_feedback)) {
            $gradebookfeedback = $grade->str_feedback;
        }
    }
}

$title = $course->shortname . ': ' . format_string($cloudexam->name);
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);
$output = $PAGE->get_renderer('mod_cloudexam');

// Print table with existing attempts.
if ($attempts) {
    // Work out which columns we need, taking account what data is available in each attempt.
    list($someoptions, $alloptions) = cloudexam_get_combined_reviewoptions($cloudexam, $attempts);

    $viewobj->attemptcolumn = $cloudexam->attempts != 1;

    $viewobj->gradecolumn  = $someoptions->marks >= question_display_options::MARK_AND_MAX &&
                             cloudexam_has_grades($cloudexam);
    $viewobj->markcolumn   = $viewobj->gradecolumn && ($cloudexam->grade != $cloudexam->sumgrades);
    $viewobj->overallstats = $lastfinishedattempt && $alloptions->marks >= question_display_options::MARK_AND_MAX;

    $viewobj->feedbackcolumn = cloudexam_has_feedback($cloudexam) && $alloptions->overallfeedback;
}

$viewobj->timenow             = $timenow;
$viewobj->numattempts         = $numattempts;
$viewobj->mygrade             = $mygrade;
$viewobj->moreattempts        = $unfinished ||
                                !$accessmanager->is_finished($numattempts, $lastfinishedattempt);
$viewobj->mygradeoverridden   = $mygradeoverridden;
$viewobj->gradebookfeedback   = $gradebookfeedback;
$viewobj->lastfinishedattempt = $lastfinishedattempt;
$viewobj->canedit             = has_capability('mod/cloudexam:manage', $context);
$viewobj->editurl             = new moodle_url('/mod/cloudexam/edit.php', array('cmid' => $cm->id));
$viewobj->backtocourseurl     = new moodle_url('/course/view.php', array('id' => $course->id));

/**EDUNAO PATCH Defines the link to the form thats sends test to cloud **/

$viewobj->configurecloudexamurl = new moodle_url('/mod/cloudexam/pages/configure_cloudexam.php', array('cmid' => $cm->id));
$viewobj->uploadcloudexamurl    = new moodle_url('/mod/cloudexam/view.php', array('action' => 'upload_exam', 'id' => $cm->id));
$viewobj->startattemptcloudexamurl = new moodle_url('/mod/cloudexam/pages/attempt_cloudexam.php', array('userid' => $USER->id, 'sitecmid' => $cm->id));



/**END EDUNAO PATCH**/

//$viewobj->startattempturl = $cloudexamobj->start_attempt_url();

if ($accessmanager->is_preflight_check_required($unfinishedattemptid)) {
    $viewobj->preflightcheckform = $accessmanager->get_preflight_check_form(
        $viewobj->startattempturl, $unfinishedattemptid);
}
$viewobj->popuprequired = $accessmanager->attempt_must_be_in_popup();
$viewobj->popupoptions  = $accessmanager->get_popup_options();

// Display information about this cloudexam.
$viewobj->infomessages = $viewobj->accessmanager->describe_rules();
if ($cloudexam->attempts != 1) {
    $viewobj->infomessages[] = get_string('gradingmethod', 'cloudexam',
        cloudexam_get_grading_option_name($cloudexam->grademethod));
}

// Determine wheter a start attempt button should be displayed.
$viewobj->cloudexamhasquestions = $cloudexamobj->has_questions();
 
$viewobj->preventmessages       = array();
if (!$viewobj->cloudexamhasquestions) {
    $viewobj->buttontext = '';
  
} else {
    if ($unfinished) {
        
        if ($canattempt) {
            $viewobj->buttontext = get_string('continueattemptcloudexam', 'cloudexam');
        } else if ($canpreview) {
            $viewobj->buttontext = get_string('continuepreview', 'cloudexam');
        }

    } else {
//       
        if ($canattempt) {
             
            $viewobj->preventmessages = $viewobj->accessmanager->prevent_new_attempt(
                $viewobj->numattempts, $viewobj->lastfinishedattempt);
            if ($viewobj->preventmessages) {
               
                $viewobj->buttontext = '';
            } else if ($viewobj->numattempts == 0) {
                
                $viewobj->buttontext = get_string('attemptcloudexamnow', 'cloudexam');
            } else {
                $viewobj->buttontext = get_string('reattemptcloudexam', 'cloudexam');
            }

        } else if ($canpreview) {
            $viewobj->buttontext = get_string('previewcloudexamnow', 'cloudexam');
        }
    }

    // If, so far, we think a button should be printed, so check if they will be
    // allowed to access it.
    if ($viewobj->buttontext) {
      
        if (!$viewobj->moreattempts) {
            
            $viewobj->buttontext = '';
        } else if ($canattempt
                   && $viewobj->preventmessages = $viewobj->accessmanager->prevent_access()) {
              
            $viewobj->buttontext = '';
        }
    }
}
// print_object($viewobj);
$viewobj->showbacktocourse = ($viewobj->buttontext === '' &&
                              course_get_format($course)->has_view_page());

echo $OUTPUT->header();

// Test eep link
if($viewobj->canedit){
     
    $testlink = \mod_cloudexam\check_eep_link(true);
    if($testlink !== true){
        echo '<div class="alert alert-danger" role="alert">' . $testlink . '</div>';
    }
}

if (isguestuser()) {
   
    // Guests can't do a cloudexam, so offer them a choice of logging in or going back.
    echo $output->view_page_guest($course, $cloudexam, $cm, $context, $viewobj->infomessages);
} else if (!isguestuser() && !($canattempt || $canpreview
                               || $viewobj->canreviewmine)) {
    
    // If they are not enrolled in this course in a good enough role, tell them to enrol.
    echo $output->view_page_notenrolled($course, $cloudexam, $cm, $context, $viewobj->infomessages);
} else {
    
    echo $output->view_page($course, $cloudexam, $cm, $context, $viewobj);
//     print_object($viewobj);die;
}

//if ($uploadresult) {
if (1) {
    echo '<p class="alert alert-success">Successful upload</p>';
}

echo $OUTPUT->footer();

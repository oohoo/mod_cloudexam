<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script lists all the instances of cloudexam in a particular course
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("../../config.php");
require_once("locallib.php");

$id = required_param('id', PARAM_INT);
$PAGE->set_url('/mod/cloudexam/index.php', array('id'=>$id));
if (!$course = $DB->get_record('course', array('id' => $id))) {
    print_error('invalidcourseid');
}
$coursecontext = context_course::instance($id);
require_login($course);
$PAGE->set_pagelayout('incourse');

$params = array(
    'context' => $coursecontext
);
$event = \mod_cloudexam\event\course_module_instance_list_viewed::create($params);
$event->trigger();

// Print the header.
$strcloudexams = get_string("modulenameplural", "cloudexam");
$PAGE->navbar->add($strcloudexams);
$PAGE->set_title($strcloudexams);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
echo $OUTPUT->heading($strcloudexams, 2);

// Get all the appropriate data.
if (!$cloudexams = get_all_instances_in_course("cloudexam", $course)) {
    notice(get_string('thereareno', 'moodle', $strcloudexams), "../../course/view.php?id=$course->id");
    die;
}

// Check if we need the closing date header.
$showclosingheader = false;
$showfeedback = false;
foreach ($cloudexams as $cloudexam) {
    if ($cloudexam->timeclose!=0) {
        $showclosingheader=true;
    }
    if (cloudexam_has_feedback($cloudexam)) {
        $showfeedback=true;
    }
    if ($showclosingheader && $showfeedback) {
        break;
    }
}

// Configure table for displaying the list of instances.
$headings = array(get_string('name'));
$align = array('left');

if ($showclosingheader) {
    array_push($headings, get_string('cloudexamcloses', 'cloudexam'));
    array_push($align, 'left');
}

if (course_format_uses_sections($course->format)) {
    array_unshift($headings, get_string('sectionname', 'format_'.$course->format));
} else {
    array_unshift($headings, '');
}
array_unshift($align, 'center');

$showing = '';

if (has_capability('mod/cloudexam:viewreports', $coursecontext)) {
    array_push($headings, get_string('attempts', 'cloudexam'));
    array_push($align, 'left');
    $showing = 'stats';

} else if (has_any_capability(array('mod/cloudexam:reviewmyattempts', 'mod/cloudexam:attempt'),
        $coursecontext)) {
    array_push($headings, get_string('grade', 'cloudexam'));
    array_push($align, 'left');
    if ($showfeedback) {
        array_push($headings, get_string('feedback', 'cloudexam'));
        array_push($align, 'left');
    }
    $showing = 'grades';

    $grades = $DB->get_records_sql_menu('
            SELECT qg.cloudexam, qg.grade
            FROM {cloudexam_grades} qg
            JOIN {cloudexam} q ON q.id = qg.cloudexam
            WHERE q.course = ? AND qg.userid = ?',
            array($course->id, $USER->id));
}

$table = new html_table();
$table->head = $headings;
$table->align = $align;

// Populate the table with the list of instances.
$currentsection = '';
// Get all closing dates.
$timeclosedates = cloudexam_get_user_timeclose($course->id);
foreach ($cloudexams as $cloudexam) {
    $cm = get_coursemodule_from_instance('cloudexam', $cloudexam->id);
    $context = context_module::instance($cm->id);
    $data = array();

    // Section number if necessary.
    $strsection = '';
    if ($cloudexam->section != $currentsection) {
        if ($cloudexam->section) {
            $strsection = $cloudexam->section;
            $strsection = get_section_name($course, $cloudexam->section);
        }
        if ($currentsection) {
            $learningtable->data[] = 'hr';
        }
        $currentsection = $cloudexam->section;
    }
    $data[] = $strsection;

    // Link to the instance.
    $class = '';
    if (!$cloudexam->visible) {
        $class = ' class="dimmed"';
    }
    $data[] = "<a$class href=\"view.php?id=$cloudexam->coursemodule\">" .
            format_string($cloudexam->name, true) . '</a>';

    // Close date.
    if ($cloudexam->timeclose) {
        if (($timeclosedates[$cloudexam->id]->usertimeclose == 0) AND ($timeclosedates[$cloudexam->id]->usertimelimit == 0)) {
            $data[] = get_string('noclose', 'cloudexam');
        } else {
            $data[] = userdate($timeclosedates[$cloudexam->id]->usertimeclose);
        }
    } else if ($showclosingheader) {
        $data[] = '';
    }

    if ($showing == 'stats') {
        // The $cloudexam objects returned by get_all_instances_in_course have the necessary $cm
        // fields set to make the following call work.
        $data[] = cloudexam_attempt_summary_link_to_reports($cloudexam, $cm, $context);

    } else if ($showing == 'grades') {
        // Grade and feedback.
        $attempts = cloudexam_get_user_attempts($cloudexam->id, $USER->id, 'all');
        list($someoptions, $alloptions) = cloudexam_get_combined_reviewoptions(
                $cloudexam, $attempts);

        $grade = '';
        $feedback = '';
        if ($cloudexam->grade && array_key_exists($cloudexam->id, $grades)) {
            if ($alloptions->marks >= question_display_options::MARK_AND_MAX) {
                $a = new stdClass();
                $a->grade = cloudexam_format_grade($cloudexam, $grades[$cloudexam->id]);
                $a->maxgrade = cloudexam_format_grade($cloudexam, $cloudexam->grade);
                $grade = get_string('outofshort', 'cloudexam', $a);
            }
            if ($alloptions->overallfeedback) {
                $feedback = cloudexam_feedback_for_grade($grades[$cloudexam->id], $cloudexam, $context);
            }
        }
        $data[] = $grade;
        if ($showfeedback) {
            $data[] = $feedback;
        }
    }

    $table->data[] = $data;
} // End of loop over cloudexam instances.

// Display the table.
echo html_writer::table($table);

// Finish the page.
echo $OUTPUT->footer();

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Definition of log events for the cloudexam module.
 *
 * @package    mod_cloudexam
 * @category   log
 * @based on   original work with copyright: 2010 Petr Skoda (http://skodak.org)
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$logs = array(
    array('module'=>'cloudexam', 'action'=>'add', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'update', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'view', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'report', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'attempt', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'submit', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'review', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'editquestions', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'preview', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'start attempt', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'close attempt', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'continue attempt', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'edit override', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'delete override', 'mtable'=>'cloudexam', 'field'=>'name'),
    array('module'=>'cloudexam', 'action'=>'view summary', 'mtable'=>'cloudexam', 'field'=>'name'),
);

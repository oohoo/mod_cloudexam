<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package    TODO
 * @subpackage TODO
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <vincent.cros@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_cloudexam;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/mod/cloudexam/lib/cloudexam_lib.php';
require_once $CFG->dirroot . '/mod/cloudexam/classes/eep_call.class.php';
require_once $CFG->dirroot . '/backup/util/includes/backup_includes.php';

use backup;
use backup_controller;
use Phar;
use PharData;
use Error;

/**
 * Class cloudexam_upload
 *
 * @package mod_cloudexam
 */
class cloudexam_upload {
    private $cmid;

    /**
     * cloudexam_upload constructor
     *
     * @param $cmid
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws \moodle_exception
     */
    public function __construct($cmid) {
        global $DB;
        if (!$DB->get_record('course_modules', array('id' => $cmid))) {
            $errormsg = get_string('error-coursemodule-notexist', 'mod_cloudexam', $cmid);
            print_error('error', 'error', '', null, $errormsg);
        }
        $this->cmid = $cmid;
    }

    /**
     * Create an activity backup and upload it on EEP
     *
     * @return bool if the activity upload on EEP worked
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws \moodle_exception
     */
    public function eep_upload_cloudexam($cmid) {

        // Create backup of the activity of this cmid
        $archivepath = $this->create_cloudexam_backup($cmid);

        // get cloudexam settings from cloudexam_lib
        $cloudexamsettings = get_exam_settings($cmid);

        if (!$cloudexamsettings) {
            $this->cleanup_archive_files($archivepath);
            $errormsg = get_string('error-coursemodule-badconfig', 'mod_cloudexam');
            print_error('error', 'error', '', null, $errormsg);
        }

        // send activity backup and its settings to EEP
        $curlcall   = new eep_call();
        $curlresult = $curlcall->upload_exam($cmid, $archivepath, $cloudexamsettings);

        if (!$curlresult) {
            $this->cleanup_archive_files($archivepath);
            $errormsg = get_string('error-cloudexamcurl-failed', 'mod_cloudexam');
            print_error('error', 'error', '', null, $errormsg);
        }

        // Delete archive files of the activity
        $this->cleanup_archive_files($archivepath);

        return true;
    }

    /**
     * Create a backups folder in the root folder
     * Create the backup of an activity then its archive
     *
     * @return string archivebackuppath the path of the backup archive
     * @throws \moodle_exception display error message
     */
    private function create_cloudexam_backup($cmid) {
        global $CFG;

        $backupsfolder = $CFG->dataroot . '/backup';

        if (!file_exists($backupsfolder)) {
            // Create a folder to put the backups where we have write permission
            mkdir($backupsfolder, 0755);
        }

        // Options for the backup controller
        $type        = backup::TYPE_1ACTIVITY;
        $format      = backup::FORMAT_MOODLE;
        $interactive = backup::INTERACTIVE_NO;
        $mode        = backup::MODE_IMPORT;

        // Create the backup
        try {
            $adminid = get_admin()->id;
            $bc      = new backup_controller($type, $cmid, $format, $interactive, $mode, $adminid);

            $bc->execute_plan();

            $backupid = $bc->get_backupid();

            // The backupid is the name of the backup folder
            $backuppath = $CFG->backuptempdir . '/' . $backupid;

            // Free memory and resources
            $bc->destroy();

        } catch (Error $e) {

            // Deleting backup folder, its content and backup log file
            shell_exec('rm -r ' . $backuppath . '/*');
            rmdir($backuppath);
            unlink($backuppath . '.log');

            $errormsg = get_string('error-cloudexambackup-creationexception', 'mod_cloudexam');
            print_error('error', 'error', '', null, $errormsg);
        }

        // Path of the backup not compressed
        $archivebackuppath = $backupsfolder . '/backup_' . $backupid . '.tar';

        // Compression of the backup
        $archive = new PharData($archivebackuppath);
        $archive->buildFromDirectory($backuppath);
        $archive->compress(Phar::GZ);
        $archivebackuppath .= '.gz';

        // Deleting backup folder, its content and backup log file
        shell_exec('rm -r ' . $backuppath . '/*');
        rmdir($backuppath);
        unlink($backuppath . '.log');

        // Check if archive backup still exists
        if (!file_exists($archivebackuppath)) {
            $errormsg = get_string('error-archivebackuppath-notfound', 'mod_cloudexam');
            print_error('error', 'error', '', null, $errormsg);
        }

        return $archivebackuppath;
    }

    /**
     * Delete archive files following $archivepath with extension '.tar' and '.tar.gz' in the backups folder
     *
     * @param string $archivepath
     * @throws \moodle_exception
     */
    private function cleanup_archive_files($archivepath) {
        if (!is_writable($archivepath)) {
            $errormsg = get_string('error-archivepath-notwritable', 'mod_cloudexam', $archivepath);
            print_error('error', 'error', '', null, $errormsg);
        }

        // Deleting backup.tar and backup.tar.gz
        unlink($archivepath);
        unlink(trim($archivepath, '.gz'));

        // Check if archive backup still exists
        if (file_exists($archivepath)) {
            $errormsg = get_string('error-archivebackup-notdeleted', 'mod_cloudexam');
            print_error('error', 'error', '', null, $errormsg);
        }
    }
}
<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package    cloudexam
 * @subpackage classes
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <vincent.cros@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_cloudexam;

/**
 * Class eep_call
 *
 * @package mod_cloudexam
 */
class eep_call {

    private $eepurl;
    private $sharedsecret;
    private $siteurl;

    public function __construct() {
        global $CFG;
        $this->eepurl = get_config('cloudexam', 'eepurl');
        $this->sharedsecret = get_config('cloudexam', 'sharedsecret');
        $this->siteurl = $CFG->wwwroot;
    }

    /**
     * Upload an exam on EEP platform
     *
     * @param $cmid
     * @param $backupfile string path of an activity archive
     * @param $settings array configuration of the exam
     * @return bool function result : false if call failed true if call function succeeded
     */
    public function upload_exam($cmid, $backupfile, $settings) {

        $data = [
            'sitecmid' => $cmid,
            'settings' => json_encode($settings)
        ];

        $files = [
            'backupfile' => $backupfile
        ];

        $callresult = $this->call('create_exam', $data, $files);
//        print_object($callresult);die;
        return $callresult;
    }

    /**
     * Call a moodle platform to send data and/or files
     *
     * @param $action string action type sent to determine the action to do on the platform called
     * @param $data array usefull informations for the platform called like cmid and settings
     * @param null $files zero, one or several files sent to the platform called
     * @return bool
     */
    private function call($action, $data, $files = null, $stopiferror = true) {
        $result = '{
    "userid" : 3, 
    "cloudexamid" : 32,
    "data" : {
        "questiondata" : {"_order":"23,21,22"}, 

        "formdata" : {"q4:1_:flagged":"0","q4:1_:sequencecheck":"1","q4:1_answer":"0"}, 

        "questionid" : 9

    }

}';

        return $result;
        $curlurl = $this->eepurl . '/local/eep/client_frontcontroller.php';

        $curl = curl_init();

        // Get required data for eep call
        $clientdata = array(
            'siteurl' => urlencode($this->siteurl),
            'sharedsecret' => $this->sharedsecret,
            'action' => $action,
        );

        $clientdata = array_merge($data, $clientdata);

        // Prepare each files for curl send
        if ($files) {
            foreach ($files as $paramname => $file) {
                $convertedfile = curl_file_create($file);
                $clientdata = array_merge($clientdata, array($paramname => $convertedfile));
            }
        }

        // Curl options and
        curl_setopt($curl, CURLOPT_URL, $curlurl);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $clientdata);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        $curlerror = curl_error($curl);
        $curlerrorno = curl_errno($curl);
        $curlinfo = curl_getinfo($curl);

        // Check whether the fetch succeeded or not
        if ($curlerror || $curlerrorno || in_array($curlinfo['http_code'], [400, 404, 500])) {
            $result = json_decode($result);
            if ($stopiferror) {
                print_error('error', 'error', '', null, $result);
                curl_close($curl);
                return false;
            }

            if(!isset($result->errors)){
                $result = new \stdClass();
                $result->errors = [];
                $result->errors[] = 'Error ' . $curlinfo['http_code'] . ' : ' . $curlerror;
            }

        }
        curl_close($curl);

        return $result;
    }

    /**
     * Attempt exam
     *
     * @param $userid
     * @param $cmid
     * @return bool
     */
    public function attempt_exam($userid, $cmid) {
        $data = [
            'userid'   => $userid,
            'sitecmid' => $cmid,
        ];

        $callresult = $this->call('attempt_exam', $data);

        if(isset($callresult->errors) || $callresult == ''){
            $errors = !isset($return->errors) ? 'Aucun retour' : json_encode($callresult->errors);
            print_error('Erreur de chargement du quiz : ' . $errors);
        }

        return $callresult;
    }

    public function check_eep_link(){
        $callresult = $this->call('check_link', [], false, false);

        return $callresult;
    }

    public function get_exam_info($cmid){
        $data = [
            'sitecmid' => $cmid,
        ];

        $callresult = $this->call('get_exam_info', $data);

        if(isset($callresult->errors) || $callresult == ''){
            $errors = !isset($return->errors) ? 'Aucun retour' : json_encode($callresult->errors);
            print_error('Erreur lors de la récupération des informations du quiz : ' . $errors);
        }
        //Force result - Kais
        $examinfo = new \stdClass();
        $examinfo->examcontentinitialised = 1;
        $examinfo->state = 1;
        $examinfo->targetstate = 1;
        return $examinfo;
        return json_decode($examinfo);
    }

    public function close_exam($cmid) {
        $data = [
            'sitecmid' => $cmid,
        ];

        $callresult = $this->call('finish_exam', $data);

        if (isset($callresult->errors) || $callresult == '') {
            $errors = !isset($return->errors) ? 'Aucun retour' : json_encode($callresult->errors);
            print_error('Erreur lors de la fermeture de l\'examen : ' . $errors);
        }

        return json_decode($callresult);
    }

}

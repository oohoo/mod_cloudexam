<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the polyfil to allow a plugin to operate with Moodle 3.3 up.
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_cloudexam\privacy;

defined('MOODLE_INTERNAL') || die();

/**
 * The trait used to provide a backwards compatibility for third-party plugins.
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
trait legacy_cloudexamaccess_polyfill {

    /**
     * Export all user data for the specified user, for the specified cloudexam.
     *
     * @param   \cloudexam           $cloudexam The cloudexam being exported
     * @param   \stdClass       $user The user to export data for
     * @return  \stdClass       The data to be exported for this access rule.
     */
    public static function export_cloudexamaccess_user_data(\cloudexam $cloudexam, \stdClass $user) : \stdClass {
        return static::_export_cloudexamaccess_user_data($cloudexam, $user);
    }

    /**
     * Delete all data for all users in the specified cloudexam.
     *
     * @param   \cloudexam           $cloudexam The cloudexam being deleted
     */
    public static function delete_cloudexamaccess_data_for_all_users_in_context(\cloudexam $cloudexam) {
        static::_delete_cloudexamaccess_data_for_all_users_in_context($cloudexam);
    }

    /**
     * Delete all user data for the specified user, in the specified cloudexam.
     *
     * @param   \cloudexam           $cloudexam The cloudexam being deleted
     * @param   \stdClass       $user The user to export data for
     */
    public static function delete_cloudexamaccess_data_for_user(\cloudexam $cloudexam, \stdClass $user) {
        static::_delete_cloudexamaccess_data_for_user($cloudexam, $user);
    }
}

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Privacy Subsystem implementation for mod_cloudexam.
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_cloudexam\privacy;

use \core_privacy\local\request\writer;
use \core_privacy\local\request\transform;
use \core_privacy\local\request\contextlist;
use \core_privacy\local\request\approved_contextlist;
use \core_privacy\local\request\deletion_criteria;
use \core_privacy\local\metadata\collection;
use \core_privacy\manager;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/lib.php');
require_once($CFG->dirroot . '/mod/cloudexam/locallib.php');

/**
 * Privacy Subsystem implementation for mod_cloudexam.
 *
 * @based on   original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements
    // This plugin has data.
    \core_privacy\local\metadata\provider,

    // This plugin currently implements the original plugin_provider interface.
    \core_privacy\local\request\plugin\provider {

    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param   collection  $items  The collection to add metadata to.
     * @return  collection  The array of metadata
     */
    public static function get_metadata(collection $items) : collection {
        // The table 'cloudexam' stores a record for each cloudexam.
        // It does not contain user personal data, but data is returned from it for contextual requirements.

        // The table 'cloudexam_attempts' stores a record of each cloudexam attempt.
        // It contains a userid which links to the user making the attempt and contains information about that attempt.
        $items->add_database_table('cloudexam_attempts', [
                'attempt'               => 'privacy:metadata:cloudexam_attempts:attempt',
                'currentpage'           => 'privacy:metadata:cloudexam_attempts:currentpage',
                'preview'               => 'privacy:metadata:cloudexam_attempts:preview',
                'state'                 => 'privacy:metadata:cloudexam_attempts:state',
                'timestart'             => 'privacy:metadata:cloudexam_attempts:timestart',
                'timefinish'            => 'privacy:metadata:cloudexam_attempts:timefinish',
                'timemodified'          => 'privacy:metadata:cloudexam_attempts:timemodified',
                'timemodifiedoffline'   => 'privacy:metadata:cloudexam_attempts:timemodifiedoffline',
                'timecheckstate'        => 'privacy:metadata:cloudexam_attempts:timecheckstate',
                'sumgrades'             => 'privacy:metadata:cloudexam_attempts:sumgrades',
            ], 'privacy:metadata:cloudexam_attempts');

        // The table 'cloudexam_feedback' contains the feedback responses which will be shown to users depending upon the
        // grade they achieve in the cloudexam.
        // It does not identify the user who wrote the feedback item so cannot be returned directly and is not
        // described, but relevant feedback items will be included with the cloudexam export for a user who has a grade.

        // The table 'cloudexam_grades' contains the current grade for each cloudexam/user combination.
        $items->add_database_table('cloudexam_grades', [
                'cloudexam'                  => 'privacy:metadata:cloudexam_grades:cloudexam',
                'userid'                => 'privacy:metadata:cloudexam_grades:userid',
                'grade'                 => 'privacy:metadata:cloudexam_grades:grade',
                'timemodified'          => 'privacy:metadata:cloudexam_grades:timemodified',
            ], 'privacy:metadata:cloudexam_grades');

        // The table 'cloudexam_overrides' contains any user or group overrides for users.
        // It should be included where data exists for a user.
        $items->add_database_table('cloudexam_overrides', [
                'cloudexam'                  => 'privacy:metadata:cloudexam_overrides:cloudexam',
                'userid'                => 'privacy:metadata:cloudexam_overrides:userid',
                'timeopen'              => 'privacy:metadata:cloudexam_overrides:timeopen',
                'timeclose'             => 'privacy:metadata:cloudexam_overrides:timeclose',
                'timelimit'             => 'privacy:metadata:cloudexam_overrides:timelimit',
            ], 'privacy:metadata:cloudexam_overrides');

        // These define the structure of the cloudexam.

        // The table 'cloudexam_sections' contains data about the structure of a cloudexam.
        // It does not contain any user identifying data and does not need a mapping.

        // The table 'cloudexam_slots' contains data about the structure of a cloudexam.
        // It does not contain any user identifying data and does not need a mapping.

        // The table 'cloudexam_reports' does not contain any user identifying data and does not need a mapping.

        // The table 'cloudexam_statistics' contains abstract statistics about question usage and cannot be mapped to any
        // specific user.
        // It does not contain any user identifying data and does not need a mapping.

        // The cloudexam links to the 'core_question' subsystem for all question functionality.
        $items->add_subsystem_link('core_question', [], 'privacy:metadata:core_question');

        // The cloudexam has two subplugins..
        $items->add_plugintype_link('cloudexam', [], 'privacy:metadata:cloudexam');
        $items->add_plugintype_link('cloudexamaccess', [], 'privacy:metadata:cloudexamaccess');

        // Although the cloudexam supports the core_completion API and defines custom completion items, these will be
        // noted by the manager as all activity modules are capable of supporting this functionality.

        return $items;
    }

    /**
     * Get the list of contexts where the specified user has attempted a cloudexam, or been involved with manual marking
     * and/or grading of a cloudexam.
     *
     * @param   int             $userid The user to search.
     * @return  contextlist     $contextlist The contextlist containing the list of contexts used in this plugin.
     */
    public static function get_contexts_for_userid(int $userid) : contextlist {
        // Get the SQL used to link indirect question usages for the user.
        // This includes where a user is the manual marker on a question attempt.
        $qubaid = \core_question\privacy\provider::get_related_question_usages_for_user('rel', 'mod_cloudexam', 'qa.uniqueid', $userid);

        // Select the context of any cloudexam attempt where a user has an attempt, plus the related usages.
        $sql = "SELECT c.id
                  FROM {context} c
                  JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
                  JOIN {modules} m ON m.id = cm.module AND m.name = :modname
                  JOIN {cloudexam} q ON q.id = cm.instance
                  JOIN {cloudexam_attempts} qa ON qa.cloudexam = q.id
             LEFT JOIN {cloudexam_overrides} qo ON qo.cloudexam = q.id AND qo.userid = :qouserid
            " . $qubaid->from . "
            WHERE (
                qa.userid = :qauserid OR
                " . $qubaid->where() . " OR
                qo.id IS NOT NULL
            ) AND qa.preview = 0
        ";

        $params = array_merge(
                [
                    'contextlevel'      => CONTEXT_MODULE,
                    'modname'           => 'cloudexam',
                    'qauserid'          => $userid,
                    'qouserid'          => $userid,
                ],
                $qubaid->from_where_params()
            );

        $resultset = new contextlist();
        $resultset->add_from_sql($sql, $params);

        return $resultset;
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param   approved_contextlist    $contextlist    The approved contexts to export information for.
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        global $DB;

        if (!count($contextlist)) {
            return;
        }

        $user = $contextlist->get_user();
        $userid = $user->id;
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);

        $sql = "SELECT
                    q.*,
                    qg.id AS hasgrade,
                    qg.grade AS bestgrade,
                    qg.timemodified AS grademodified,
                    qo.id AS hasoverride,
                    qo.timeopen AS override_timeopen,
                    qo.timeclose AS override_timeclose,
                    qo.timelimit AS override_timelimit,
                    c.id AS contextid,
                    cm.id AS cmid
                  FROM {context} c
            INNER JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
            INNER JOIN {modules} m ON m.id = cm.module AND m.name = :modname
            INNER JOIN {cloudexam} q ON q.id = cm.instance
             LEFT JOIN {cloudexam_overrides} qo ON qo.cloudexam = q.id AND qo.userid = :qouserid
             LEFT JOIN {cloudexam_grades} qg ON qg.cloudexam = q.id AND qg.userid = :qguserid
                 WHERE c.id {$contextsql}";

        $params = [
            'contextlevel'      => CONTEXT_MODULE,
            'modname'           => 'cloudexam',
            'qguserid'          => $userid,
            'qouserid'          => $userid,
        ];
        $params += $contextparams;

        // Fetch the individual cloudexams.
        $cloudexams = $DB->get_recordset_sql($sql, $params);
        foreach ($cloudexams as $cloudexam) {
            list($course, $cm) = get_course_and_cm_from_cmid($cloudexam->cmid, 'cloudexam');
            $cloudexamobj = new \cloudexam($cloudexam, $cm, $course);
            $context = $cloudexamobj->get_context();

            $cloudexamdata = \core_privacy\local\request\helper::get_context_data($context, $contextlist->get_user());
            \core_privacy\local\request\helper::export_context_files($context, $contextlist->get_user());

            if (!empty($cloudexamdata->timeopen)) {
                $cloudexamdata->timeopen = transform::datetime($cloudexam->timeopen);
            }
            if (!empty($cloudexamdata->timeclose)) {
                $cloudexamdata->timeclose = transform::datetime($cloudexam->timeclose);
            }
            if (!empty($cloudexamdata->timelimit)) {
                $cloudexamdata->timelimit = $cloudexam->timelimit;
            }

            if (!empty($cloudexam->hasoverride)) {
                $cloudexamdata->override = (object) [];

                if (!empty($cloudexamdata->override_override_timeopen)) {
                    $cloudexamdata->override->timeopen = transform::datetime($cloudexam->override_timeopen);
                }
                if (!empty($cloudexamdata->override_timeclose)) {
                    $cloudexamdata->override->timeclose = transform::datetime($cloudexam->override_timeclose);
                }
                if (!empty($cloudexamdata->override_timelimit)) {
                    $cloudexamdata->override->timelimit = $cloudexam->override_timelimit;
                }
            }

            $cloudexamdata->accessdata = (object) [];

            $components = \core_component::get_plugin_list('cloudexamaccess');
            $exportparams = [
                    $cloudexamobj,
                    $user,
                ];
            foreach (array_keys($components) as $component) {
                $classname = manager::get_provider_classname_for_component("cloudexamaccess_$component");
                if (class_exists($classname) && is_subclass_of($classname, cloudexamaccess_provider::class)) {
                    $result = component_class_callback($classname, 'export_cloudexamaccess_user_data', $exportparams);
                    if (count((array) $result)) {
                        $cloudexamdata->accessdata->$component = $result;
                    }
                }
            }

            if (empty((array) $cloudexamdata->accessdata)) {
                unset($cloudexamdata->accessdata);
            }

            writer::with_context($context)
                ->export_data([], $cloudexamdata);
        }
        $cloudexams->close();

        // Store all cloudexam attempt data.
        static::export_cloudexam_attempts($contextlist);
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param   context                 $context   The specific context to delete data for.
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        if ($context->contextlevel != CONTEXT_MODULE) {
            // Only cloudexam module will be handled.
            return;
        }

        $cm = get_coursemodule_from_id('cloudexam', $context->instanceid);
        if (!$cm) {
            // Only cloudexam module will be handled.
            return;
        }

        $cloudexamobj = \cloudexam::create($cm->instance);
        $cloudexam = $cloudexamobj->get_cloudexam();

        // Handle the 'cloudexamaccess' subplugin.
        manager::plugintype_class_callback(
                'cloudexamaccess',
                cloudexamaccess_provider::class,
                'delete_subplugin_data_for_all_users_in_context',
                [$cloudexamobj]
            );

        // Delete all overrides - do not log.
        cloudexam_delete_all_overrides($cloudexam, false);

        // This will delete all question attempts, cloudexam attempts, and cloudexam grades for this cloudexam.
        cloudexam_delete_all_attempts($cloudexam);
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param   approved_contextlist    $contextlist    The approved contexts and user information to delete information for.
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $DB;

        foreach ($contextlist as $context) {
            if ($context->contextlevel != CONTEXT_MODULE) {
            // Only cloudexam module will be handled.
                continue;
            }

            $cm = get_coursemodule_from_id('cloudexam', $context->instanceid);
            if (!$cm) {
                // Only cloudexam module will be handled.
                continue;
            }

            // Fetch the details of the data to be removed.
            $cloudexamobj = \cloudexam::create($cm->instance);
            $cloudexam = $cloudexamobj->get_cloudexam();
            $user = $contextlist->get_user();

            // Handle the 'cloudexamaccess' cloudexamaccess.
            manager::plugintype_class_callback(
                    'cloudexamaccess',
                    cloudexamaccess_provider::class,
                    'delete_cloudexamaccess_data_for_user',
                    [$cloudexamobj, $user]
                );

            // Remove overrides for this user.
            $overrides = $DB->get_records('cloudexam_overrides' , [
                'cloudexam' => $cloudexamobj->get_cloudexamid(),
                'userid' => $user->id,
            ]);

            foreach ($overrides as $override) {
                cloudexam_delete_override($cloudexam, $override->id, false);
            }

            // This will delete all question attempts, cloudexam attempts, and cloudexam grades for this cloudexam.
            cloudexam_delete_user_attempts($cloudexamobj, $user);
        }
    }

    /**
     * Store all cloudexam attempts for the contextlist.
     *
     * @param   approved_contextlist    $contextlist
     */
    protected static function export_cloudexam_attempts(approved_contextlist $contextlist) {
        global $DB;

        $userid = $contextlist->get_user()->id;
        list($contextsql, $contextparams) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);
        $qubaid = \core_question\privacy\provider::get_related_question_usages_for_user('rel', 'mod_cloudexam', 'qa.uniqueid', $userid);

        $sql = "SELECT
                    c.id AS contextid,
                    cm.id AS cmid,
                    qa.*
                  FROM {context} c
                  JOIN {course_modules} cm ON cm.id = c.instanceid AND c.contextlevel = :contextlevel
                  JOIN {modules} m ON m.id = cm.module AND m.name = 'cloudexam'
                  JOIN {cloudexam} q ON q.id = cm.instance
                  JOIN {cloudexam_attempts} qa ON qa.cloudexam = q.id
            " . $qubaid->from. "
            WHERE (
                qa.userid = :qauserid OR
                " . $qubaid->where() . "
            ) AND qa.preview = 0
        ";

        $params = array_merge(
                [
                    'contextlevel'      => CONTEXT_MODULE,
                    'qauserid'          => $userid,
                ],
                $qubaid->from_where_params()
            );

        $attempts = $DB->get_recordset_sql($sql, $params);
        foreach ($attempts as $attempt) {
            $cloudexam = $DB->get_record('cloudexam', ['id' => $attempt->cloudexam]);
            $context = \context_module::instance($attempt->cmid);
            $attemptsubcontext = helper::get_cloudexam_attempt_subcontext($attempt, $contextlist->get_user());
            $options = cloudexam_get_review_options($cloudexam, $attempt, $context);

            if ($attempt->userid == $userid) {
                // This attempt was made by the user.
                // They 'own' all data on it.
                // Store the question usage data.
                \core_question\privacy\provider::export_question_usage($userid,
                        $context,
                        $attemptsubcontext,
                        $attempt->uniqueid,
                        $options,
                        true
                    );

                // Store the cloudexam attempt data.
                $data = (object) [
                    'state' => \cloudexam_attempt::state_name($attempt->state),
                ];

                if (!empty($attempt->timestart)) {
                    $data->timestart = transform::datetime($attempt->timestart);
                }
                if (!empty($attempt->timefinish)) {
                    $data->timefinish = transform::datetime($attempt->timefinish);
                }
                if (!empty($attempt->timemodified)) {
                    $data->timemodified = transform::datetime($attempt->timemodified);
                }
                if (!empty($attempt->timemodifiedoffline)) {
                    $data->timemodifiedoffline = transform::datetime($attempt->timemodifiedoffline);
                }
                if (!empty($attempt->timecheckstate)) {
                    $data->timecheckstate = transform::datetime($attempt->timecheckstate);
                }

                if ($options->marks == \question_display_options::MARK_AND_MAX) {
                    $grade = cloudexam_rescale_grade($attempt->sumgrades, $cloudexam, false);
                    $data->grade = (object) [
                            'grade' => cloudexam_format_grade($cloudexam, $grade),
                            'feedback' => cloudexam_feedback_for_grade($grade, $cloudexam, $context),
                        ];
                }

                writer::with_context($context)
                    ->export_data($attemptsubcontext, $data);
            } else {
                // This attempt was made by another user.
                // The current user may have marked part of the cloudexam attempt.
                \core_question\privacy\provider::export_question_usage(
                        $userid,
                        $context,
                        $attemptsubcontext,
                        $attempt->uniqueid,
                        $options,
                        false
                    );
            }
        }
        $attempts->close();
    }
}

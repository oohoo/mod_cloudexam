<?php


// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @package    cloudexam
 * @subpackage classes
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <vincent.cros@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_cloudexam;

class eep_call {

    private $eepurl;
    private $sharedsecret;
    private $siteurl;

    public function __construct() {
        //TODO récupérer adresse EEP, shared secret, @ site actuel
    }
    
    public function put_exam($paramexplicite){
    
    }

    /**
     * @param string $action nom fonction
     * @param array $data
     */
    private function call($action, $data) {

        //TODO init cURL
        //TODO faire appel
        //TODO gestion d'erreurs : serveur de répond pas, serveur erreur, actions pas bonnes, autres messages [type message]
        //TODO renvoyer réponse
    }


}

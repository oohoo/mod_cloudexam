<?php

/*
 * Author:  Kais Abid - Oohoo IT Services Inc. https://oohoo.biz                                         
 * The purpose of this class is to call the couldexam web services based on the data returned by eep call from the 
 * cloudexam server.
 * Thus populating the DB with the results that have been obtained on the remote server and conserving the attempts data. 
 * For this to work every instance that uses the cloudexam needs the following:
 * -A Web service user 
 * -A generated WS token to be used by this plugin for the ws calls
 * 
 * 
 * The WS user has to be enrolled in the course where cloudexam is to be run OR core changes have to be made. The former is recommended
 * There is a function that takes care of this
 *  
 * Another function was added to the cloudexam web service that allows a user (the WS user) to start or finish an attempt on behalf 
 * of another user. Added: start_attempt_modified() and modified: validate_attempt() 
 * Naturally for this to work the cloudexam functions have to be added to the Web service and the WS be added to the Authorised users.
 * 
 * cloudexam_prepare_and_start_new_attempt() in locallib.php has been edited to accept a userID so we can start the attempt on behalf of another user.  
 *  
 * 2 Settings have been added: path to json file and wsuserid 
 *
 * For the time being only the process attempt is being used because the data is being reconciled before it gets here and the json already
 * has the IDs of the cloudexam and its questions.
 * 
 */

namespace mod_cloudexam;

require_once($CFG->dirroot . '/webservice/lib.php');
require_once($CFG->dirroot . '/user/lib.php');

class process_eep_json {

    private $url;
    private $wstoken;
    private $wsuserid;
    private $cloudexamid;
    private $userid;
    private $attemptid;
    private $questionids;
    private $attemptobject;
    private $numberofquestions;
    private $eeporder;
    private $eepanswers;
    private $json;
    private $context;

    const WSSHORTNAME = 'cloudexamExternalService';
    const WSROLESHORTNAME = 'ws-cloudexam';
    const WSFUNCTION = 'mod_cloudexam_';
    const FORMAT = 'json';

    function __construct($json = '') {
        global $CFG;
        $this->context = \context_system::instance();
        $this->ws_init();
        $this->enrol_wsuser_in_cloudexam_courses();
        $this->url = $CFG->wwwroot . '/webservice/rest/server.php';
        if ($json == '') {
            $this->json = json_decode(file_get_contents(get_config('cloudexam', 'jsonpath')));
        } else {
            $this->json = json_decode($json);
        }
        $this->eeporder = [];
        $this->eepanswers = [];
        $this->questionids = [];
        foreach ($this->json as $data) {
            //Clear attempt ID when looping
           $this->attemptid = null;
            $this->userid = $data->userid;
            $this->cloudexamid = $data->cloudexamid;
            //If there's an ongoing attempt, grab it and continue with it
            $this->get_unfinished_user_attempts();
            if (!$this->attemptid) {
                //no inprogress attempt. Create one and capture the attempt ID
                $this->start_attempt();
            }

//        Returns the number of questions in the cloudexam
            $this->numberofquestions = count($data->data);
            //do some parsing
            foreach ($data->data as $questiondata) {

                array_push($this->eeporder, explode(',', $questiondata->questiondata->_order));
                array_push($this->eepanswers, array_values((array) $questiondata->formdata)[2]);
                array_push($this->questionids, array_values((array) $questiondata->questionid)[0]);
            }
            $this->process_engine();
            
        }
    }

    /**
     * Returns how many question there is in a given cloudexam
     * Fetched in Constructor. Should be deleted. 
     * @global \moodle_database $DB
     */
    function get_number_of_questions() {
        global $DB;
        $this->numberofquestions = $DB->count_records('cloudexam_slots', ['cloudexamid' => $this->cloudexamid]);
    }

    /**
     * Starts the attempt and generates the attemptid and page and saves them in the class variables
     */
    function start_attempt() {
        $function = 'start_attempt_modified'; // This is the modified version of start_attempt, accepts the userid
        $params = [
            'wstoken' => $this->wstoken,
            'wsfunction' => self::WSFUNCTION . $function,
            'moodlewsrestformat' => self::FORMAT,
            'cloudexamid' => $this->cloudexamid,
            'userid' => $this->userid,
        ];
        $param_string = http_build_query($params, '', '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $decoded = json_decode($output);
        $this->attemptobject = $decoded;
        $this->attemptid = $decoded->attempt->id;
        curl_close($ch);
    }

    /**
     * 
     * @global \moodle_database $DB
     * Find the order used on the EEP
     * @param type $qid
     * @param type $cloudexamattempt
     */
    function get_answers_order($questionid, $cloudexamattempt) {
        global $DB;
        $questionattemptid = $DB->get_record('question_attempts', ['questionid' => $questionid, 'questionusageid' => $cloudexamattempt]);
        $questionattemptstepid = $DB->get_record('question_attempt_steps', ['questionattemptid' => $questionattemptid->id, 'state' => 'todo']);
        $order = $DB->get_record('question_attempt_step_data', ['attemptstepid' => $questionattemptstepid->id, 'name' => '_order']);
        $strorder = explode(',', $order->value);
        return $strorder;
    }

    /**
     * 
     * @param type $data
     * @param type $qid
     * @param type $finishattempt
     * @param type $preflightdataoff
     */
    function process_attempt($data, $qid, $finishattempt = false, $preflightdataoff = false) {

        global $CFG;
        $function = 'process_attempt';
        $preflightdata = ['name' => 'confirmdatasaved', 'value' => 1];
        $params = [
            'wstoken' => $this->wstoken,
            'moodlewsrestformat' => self::FORMAT,
            'wsfunction' => 'mod_cloudexam_' . $function,
            'attemptid' => $this->attemptid,
            'data' => $data,
            'finishattempt' => $finishattempt,
            'timeup' => false,
            'preflightdata' => [
                $preflightdataoff == true ? [] : $preflightdata,
            ],
        ];
        $paramstring = http_build_query($params, '', '&');
        $dir = $CFG->dataroot . '/cloudexam/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramstring);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $jsondecodedoutput = json_decode($output);
        if (isset($jsondecodedoutput->exception)) {
            if (!file_exists($dir)) {
                $old = umask(0);
                mkdir($dir, 0777);
                umask($old);
            }
            $logpath = $dir . 'log.txt';
            $params['questionid'] = $qid;
            file_put_contents($logpath, date("Y-m-d H:i:s", time()) . ':  ', FILE_APPEND);
            file_put_contents($logpath, 'User ID: ' . $this->userid . " --- ", FILE_APPEND);
            file_put_contents($logpath, 'Cloudexam ID: ' . $this->cloudexamid . " --- ", FILE_APPEND);
            file_put_contents($logpath, 'Attempt ID: ' . $this->attemptid . " --- ", FILE_APPEND);
            file_put_contents($logpath, 'Question ID: ' . $this->questionids[$qid - 1] . "\r\n", FILE_APPEND);
            file_put_contents($logpath, json_encode($output) . "\r\n", FILE_APPEND);
        }
        curl_close($ch);
    }

    /**
     * Provided with the right data this function processes the attempt by answering the questions one by one until finishing the attempt
     */
    function process_engine() {
        $j = 0;
        for ($i = 1; $i < $this->numberofquestions + 1; $i++) { //Questions start from 1
            $order = $this->get_answers_order($this->questionids[$i - 1], $this->attemptid);

            if (isset($this->eeporder[$i - 1][$this->eepanswers[$i - 1]])) {
                $selectedvalue = $this->eeporder[$i - 1][$this->eepanswers[$i - 1]];
                $selectedvaluekey = array_search($selectedvalue, $order);
            } else {
                $selectedvaluekey = NULL; // In case answer is empty
            }

            $data = [
                ['name' => 'q' . $this->attemptid . ':' . $i . '_:sequencecheck', 'value' => 1], //This is hard coded because that's how it should be returned if everything went well. 
                ['name' => 'q' . $this->attemptid . ':' . $i . '_answer', 'value' => $selectedvaluekey] //Answers array starts from 0
            ];
            $this->process_attempt($data, $i);
            $j = $i;
        }
        //finish the attempt
        $this->process_attempt([], $j, true, true);
    }

    /**
     * 
     */
    function get_unfinished_user_attempts() {
        $function = 'get_user_attempts';
        $params = [
            'wstoken' => $this->wstoken,
            'wsfunction' => 'mod_cloudexam_' . $function,
            'moodlewsrestformat' => self::FORMAT,
            'cloudexamid' => $this->cloudexamid,
            'userid' => $this->userid,
            'status' => "unfinished",
            'includepreviews' => true
        ];
        $param_string = http_build_query($params, '', '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = json_decode(curl_exec($ch));
        if (count($output->attempts) > 0) {
            $this->attemptid = $output->attempts[0]->id;
        }

        curl_close($ch);
    }

    /**
     * Enrol Web service user in all courses where there is a cloudexam
     * @global \moodle_database $DB
     */
    function enrol_wsuser_in_cloudexam_courses() {
        global $DB;
        $sql = 'SELECT DISTINCT(course) FROM {cloudexam}';
        $courses = $DB->get_records_sql($sql);
        $cloudexamcourses = (array_keys($courses));
        foreach ($cloudexamcourses as $courseid) {
            $context = \context_course::instance($courseid);
            if (!is_enrolled($context, $this->wsuserid)) {
                enrol_try_internal_enrol($courseid, $this->wsuserid);
            }
        }
    }

    /**
     * Enables web services, 
     * If not already done: creates:
     * -WS user, 
     * -WS role, 
     * -assigns functions to external WS, 
     * -creates token, 
     * -updates plugin settings, 
     * -assigns authorized users
     * @global \moodle_database $DB
     */
    function ws_init() {
        global $DB;
        $systemcontext = $this->context;

        //Enable Web Services and REST protocol.
        set_config('enablewebservices', true);
        set_config('webserviceprotocols', 'rest');

        // Create a web service user if it doesn't already exist.
        if ($DB->record_exists('user', ['email' => 'ws@cloudexam.cloudexam'])) {
            $user = $DB->get_record('user', ['email' => 'ws@cloudexam.cloudexam']);
            $this->wsuserid = $user->id;
        } else {
            $user = [
                'username' => 'ws1',
                'email' => 'ws@cloudexam.cloudexam',
                'firstname' => 'Cloudexam',
                'lastname' => 'User',
                'auth' => 'webservice',
                'mnethostid' => 1,
                'confirmed' => 1,
                'timemodified' => time()
            ];
            $this->wsuserid = user_create_user($user);
        }
        // Create a web service role if it doesn't already exist.
        if ($DB->record_exists('role', ['shortname' => self::WSROLESHORTNAME])) {
            $wsrole = $DB->get_record('role', ['shortname' => self::WSROLESHORTNAME]);
            $wsroleid = $wsrole->id;
        } else {
            $wsroleid = create_role('WS Role for cloudexam', self::WSROLESHORTNAME, '');
        }
        //Assign capabilities if not already done
        set_role_contextlevels($wsroleid, [CONTEXT_SYSTEM]);
        assign_capability('webservice/rest:use', CAP_ALLOW, $wsroleid, $systemcontext->id, true);
        assign_capability('mod/cloudexam:view', CAP_ALLOW, $wsroleid, $systemcontext->id, true);
        assign_capability('mod/cloudexam:attempt', CAP_ALLOW, $wsroleid, $systemcontext->id, true);
        assign_capability('mod/cloudexam:reviewmyattempts', CAP_ALLOW, $wsroleid, $systemcontext->id, true);
        assign_capability('mod/cloudexam:viewreports', CAP_ALLOW, $wsroleid, $systemcontext->id, true);

        // Assign the wsrole to the wsuser.
        role_assign($wsroleid, $this->wsuserid, $systemcontext->id);

        // Create and enable the new webservice.
        $webservicemanager = new \webservice();
        $service = $webservicemanager->get_external_service_by_shortname(self::WSSHORTNAME);

        if (!$service) {
            $service = new \stdClass();
            $service->name = 'cloudexamExternalService';
            $service->enabled = 1;
            $service->restrictedusers = 1;
            $service->shortname = self::WSSHORTNAME;
            $service->timecreated = time();
            $service->timemodified = time();
            $service->downloadfiles = 0;
            $service->uploadfiles = 0;

            $service = $webservicemanager->add_external_service($service);
        }
        //Added required functions. Remove what's there and start fresh
        $servicefunctions = $DB->count_records('external_services_functions', ['externalserviceid' => $service->id]);
        if ($servicefunctions > 0) {
            $servicefunctions = $DB->get_records('external_services_functions', ['externalserviceid' => $service->id]);
            foreach ($servicefunctions as $sf) {
                $webservicemanager->remove_external_function_from_service($sf->functionname, $service->id);
            }
        }

        $webservicemanager->add_external_function_to_service('mod_cloudexam_get_attempt_data', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_get_user_attempts', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_get_attempt_review', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_get_attempt_summary', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_get_cloudexams_by_courses', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_get_attempt_access_information', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_process_attempt', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_save_attempt', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_start_attempt_modified', $service->id);
        $webservicemanager->add_external_function_to_service('mod_cloudexam_view_attempt', $service->id);

        $service_updated = $webservicemanager->get_external_service_by_shortname(self::WSSHORTNAME);

        // Authorise the user to use the service if that's not already done.
        if (!$DB->record_exists('external_services_users', ['externalserviceid' => $service->id,
                    'userid' => $this->wsuserid])) {
            $webservicemanager->add_ws_authorised_user((object) ['externalserviceid' => $service->id,
                        'userid' => $this->wsuserid]);
        }

        // Create a token for the user, if it doesn't exist.
        $sql = "SELECT * FROM {external_tokens} where externalserviceid = ? and userid = ? and contextid = ? and (validuntil = 0 OR validuntil > ?)";
        if ($DB->record_exists_sql($sql, ['externalserviceid' => $service->id, 'userid' => $this->wsuserid, 'contextid' => $systemcontext->id, 'validuntil' => time()])) {
            $token = $DB->get_record_sql($sql, ['externalserviceid' => $service->id, 'userid' => $this->wsuserid, 'contextid' => $systemcontext->id, 'validuntil' => time()]);

            $this->wstoken = $token->token;
        } else {
            $token = external_generate_token(EXTERNAL_TOKEN_PERMANENT, $service_updated, $this->wsuserid, $systemcontext);
            $this->wstoken = $token;
        }

        //We have to update this settings as it is checked in the validate_attempt() later on. 
        //This way we allow the WS user to process attempts that belongs to other users
        set_config('wsid', $this->wsuserid, 'cloudexam');
    }

}

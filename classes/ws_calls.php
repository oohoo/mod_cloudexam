<?php

/*
 * The purpose of this class is to call the couldexam web services based on the data returned by eep call from the 
 * cloudexam server.
 * Thus populating the DB with the results that have been obtained on the remote server and conserving the attempts data. 
 * For this to work every instance that uses the cloudexam needs to generate a ws token to be used by this plugin for the 
 * ws calls
 * 
 * This class will start the attempt and then get the formatted data from the EEP and push the corresponding appropriate data from the start attempt  
 * 
 * The WS user has to be enrolled in the course where cloudexam is to be run. OR core changes have to be made
 * Generate a list of courses that have a cloud exam instance 
 * The WS core_enrol_get_users_courses has to be called to check if the user is enrolled in the list of courses where 
 * there is an instance of cloudexam 
 * The WS enrol_manual_enrol_users will be used to enrol the WS user across all courses.
 * 
 * While this is being written, quiz is used. It should be replaced with cloudexam


 */

namespace mod_cloudexam;

class ws_calls {

    private $url;
    private $wstoken;
    private $quizid;
    private $userid;
    private $attemptid;
    private $page;
    private $squencecheck;
    private $attemptobject;
    private $number_of_questions;
    private $eep_order;
    private $eep_answers;
    private $json;

    const wsfunction = 'mod_quiz_';
    const format = 'json';

    function get_json() {
//        return file_get_contents(get_config('cloudexam', 'jsonpath'));
        return '{
    "3": {
        "userid": "3",
        "cloudexamid": "15368",
        "data": [
            {
                "questionid": "247",
                "questiondata": {
                    "__order": "124,123,125"
                },
                "formdata": {
                    "q18:1_:flagged": "0",
                    "q18:1_:sequencecheck": "1",
                    "q18:1_answer": "1"
                }
            },
            {
                "questionid": "248",
                "questiondata": {
                    "__order": "126,128,127,129,130"
                },
                "formdata": {
                    "q18:2_:flagged": "0",
                    "q18:2_:sequencecheck": "1",
                    "q18:2_answer": "3"
                }
            },
            {
                "questionid": "249",
                "questiondata": {
                    "__order": "131,132,133"
                },
                "formdata": {
                    "q18:3_:flagged": "0",
                    "q18:3_:sequencecheck": "1",
                    "q18:3_answer": "0"
                }
            }
        ]
    }
}
';
    }

    function __construct($json, $url, $wstoken, $quizid = 0, $userid = 0, $attemptid = 0) {
        global $DB;
        $this->url = $url;
        $this->wstoken = $wstoken;
//        $this->json = json_decode($this->get_json());
        $this->json = json_decode(file_get_contents(get_config('cloudexam', 'jsonpath')));
         $this->eep_answers = [];
//        print_object($this->json);
//        die;
//        $this->json = json_decode('{
//    "userid" : 3, 
//    "cloudexamid" : 32,
//    "data" : {
//        "questiondata" : {"_order":"23,21,22"}, 
//
//        "formdata" : {"q4:1_:flagged":"0","q4:1_:sequencecheck":"1","q4:1_answer":"0"}, 
//
//        "questionid" : 9
//
//    }
//
//}');
        foreach ($this->json as $data) {
//            print_object($data);
//            die;
            $this->userid = $data->userid;
            $this->quizid = $data->cloudexamid;
            $this->attemptid = $this->get_attemptid_from_json($data->data[0]->formdata);

//        Returns the number of questions in the quiz
//            $this->number_of_questions = $DB->count_records('quiz_slots', ['quizid' => $this->quizid]);
            $this->number_of_questions = count($data->data);
            //do some parsing
            foreach ($data->data as $question_data) {
//                 print_object($question_data);
//                    get_attemptid_from_json($question_data->formdata);


                $this->eep_order[$question_data->questionid] = explode(',', $question_data->questiondata->_order);
                
//                array_push($this->eep_order[$question_data->questionid], array_values((array)$question_data->formdata)[2]);
                
                 array_push( $this->eep_answers, array_values((array)$question_data->formdata)[2]);
//                print_object(array_values((array)$question_data->formdata)[2]);
//                print_object($this->eep_answers);
            }
//        $this->eep_order = [
//            4,
//            5,
//            6
//        ];
//            print_object($this);
//            die;
        }
//        for ($i = 0; $i < $this->number_of_questions; $i++) {
            $this->process_engine();
//        }
    }

    function getJson() {
        return $this->json;
    }

    function getEep_order() {
        return $this->eep_order;
    }

    function get_attemptid_from_json($formdata) {
        $formdata_array = (array_keys((array) $formdata));
        $str = $formdata_array[0];
        $delim = strpos($str, ':');
        $attemptid = substr($str, 1, $delim - 1);
        return $attemptid;
    }

    /**
     * Returns how many question there is in a given quiz
     * Fetched in Constructor. Should be deleted. 
     * @global \moodle_database $DB
     */
    function get_number_of_questions() {
//        quiz_get_coursemodule_info(2);
        global $DB;
//        should be replaced with cloudexam
        $this->number_of_questions = $DB->count_records('quiz_slots', ['quizid' => $this->quizid]);
    }

    function get_quiz_access_information() {
        $function = 'get_quiz_access_information';
        $params = [
            'wstoken' => $this->wstoken,
            'moodlewsrestformat' => self::format,
            'wsfunction' => wsfunction . $function,
            'quizid' => 1
        ];
        $param_string = http_build_query($params, '', '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        print_object('***********get_quiz_access_information*********');
        //    print_object('$output');
        $output = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Starts the attempt and generates the attemptid and page and saves them in the class variables
     */
    function start_attempt() {
        $function = 'start_attempt_modified'; // This is the modified version of start_attempt, accepts the userid
//    print_object('$output');
        $params = [
            'wstoken' => $this->wstoken,
            'wsfunction' => self::wsfunction . $function,
            'moodlewsrestformat' => self::format,
            'quizid' => $this->quizid,
            'userid' => $this->userid,
        ];
        $param_string = http_build_query($params, '', '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        curl_setopt($ch, CURLOPT_URL, url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
//        print_object('***********start_attempt*********');
//        print_object($output);
        $decoded = json_decode($output);
        $this->attemptobject = json_decode($output);
        $this->attemptid = $decoded->attempt->id;
        $this->page = $decoded->attempt->currentpage;
//        $page = $decoded->attempt->currentpage;
//        print_object('********start_attempt****** DECODED ');
//        print_object($decoded);
//        print_object($attemptid);
//        print_object($page);
        curl_close($ch);
    }

    /**
     * Uses the attemptid and page from the start_attempt function and retrieves the sequence check and stores it into the class variable
     */
    function get_attempt_data() {
        $function = 'get_attempt_data';
        $params = [
            'wstoken' => $this->wstoken,
            'wsfunction' => self::wsfunction . $function,
            'moodlewsrestformat' => self::format,
            'quizid' => $this->quizid,
            'attemptid' => $this->attemptid,
            'page' => $this->page,
            'preflightdata' => [
                ['name' => 'confirmdatasaved', 'value' => 1] //This is hard coded. Need to find out how to read from the preflight data but most likely will always be true otheriwse won't proceed 
            ],
        ];
        $param_string = http_build_query($params, '', '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        print_object('***********get_attempt_data*********');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $decoded = json_decode($output);
        $squencecheck = $decoded->questions;
        $seqCheck = [];
        foreach ($squencecheck as $seq) {
            array_push($seqCheck, $seq->sequencecheck);
        }
        $this->squencecheck = $seqCheck;
    }

    function process_attempt($attemptid, $data, $finishattempt = false, $preflightdata_off = false) {
        $function = 'process_attempt';
        $preflightdata = ['name' => 'confirmdatasaved', 'value' => 1];
        $params = [
            'wstoken' => $this->wstoken, // 'ecea25f6e4cb1e259d2843e140a47dd5',
            'moodlewsrestformat' => self::format,
            'wsfunction' => 'mod_quiz_' . $function,
            'attemptid' => $this->attemptid,
            'data' => $data,
            'finishattempt' => $finishattempt,
            'timeup' => false,
            'preflightdata' => [
                $preflightdata_off == true ? [] : $preflightdata,
            ],
        ];
        $param_string = http_build_query($params, '', '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        print_object('***********process_attempt*********');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        print_object($output);
        curl_close($ch);
    }

    /**
     * 
     * @global \moodle_database $DB
     * Find the order used on the EEP
     * @param type $qid
     * @param type $quizattempt
     */
    function get_answers_order($questionid, $quizattempt) {
        global $DB;
        $questionattemptid = $DB->get_record('question_attempts', ['questionid' => $questionid, 'questionusageid' => $quizattempt]); //1
        $questionattemptstepid = $DB->get_record('question_attempt_steps', ['questionattemptid' => $questionattemptid->id, 'state' => 'todo']); //2
        $order = $DB->get_record('question_attempt_step_data', ['attemptstepid' => $questionattemptstepid->id, 'name' => '_order']);

        $str_order = explode(',', $order->value);
//        print_object($str_order);
        return $str_order;
    }

    function process_engine() {
//        $qid; // This should be provided by the json of eep
        for ($i = 1; $i < $this->number_of_questions + 1; $i++) { //Questions start from 1
//             ['name' => 'q1:5_:sequencecheck', 'value' => 1], //This is hard coded. Need to find out how to read the preflight data
//            ['name' => 'q1:5_answer', 'value' => 2] //This is hard coded. Need to find out how to read the preflight data
//            
            //Find the selected answer in eep and select it from get_answers_order() while being mindful of the array key3
            $order = get_answers_order($qid, $this->attemptid);
//            $selected_value = array_search($this->eep_order['ANSWER_IN_FORMDATA_JSON'], $order);
            $selected_value = array_search($this->eep_order['ANSWER_IN_FORMDATA_JSON'], $order);
            $data = [
                ['name' => 'q' . $this->attemptid . ':' . $i . '_:sequencecheck', 'value' => 1], //This is hard coded. Need to find out how to read the preflight data
                ['name' => 'q' . $this->attemptid . ':' . $i . '_answer', 'value' => $this->eep_answers[$i-1]] //Answers array starts from 0
            ];
//            print_object($data);
            $this->process_attempt($this->attemptid, $data);die;
        }  
//        print_object($data);
//        die;
        //finish the attempt
        $this->process_attempt($this->attemptid, [], true, true);
    }

}

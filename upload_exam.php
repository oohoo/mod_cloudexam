<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *sends data from html attempt forms to cluster
 *
 * @package    mod
 * @subpackage cloudexam
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <vincent.cros@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$script  = '';
$script .= 'var xhttp = new XMLHttpRequest();';
$script .= 'xhttp.open("POST","http://localhost/mod/cloudexam/pages/dummypage.php", true);';
$script .= 'var myForm = document.forms[\'upload_answer\'];';
$script .= 'xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");';
$script .= 'xhttp.send(myForm);';

echo '<script>' . $script . '</script>';

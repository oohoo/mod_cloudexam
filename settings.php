<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration settings definitions for the cloudexam module.
 *
 * @package   mod_cloudexam
 * @based on  original work with copyright: 2010 Petr Skoda
 * @copyright 2019 onwards Edunao SA
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/lib.php');

// First get a list of cloudexam reports with there own settings pages. If there none,
// we use a simpler overall menu structure.
$reports = core_component::get_plugin_list_with_file('cloudexam', 'settings.php', false);
$reportsbyname = array();
foreach ($reports as $report => $reportdir) {
    $strreportname = get_string($report . 'report', 'cloudexam_'.$report);
    $reportsbyname[$strreportname] = $report;
}
core_collator::ksort($reportsbyname);

// First get a list of cloudexam reports with there own settings pages. If there none,
// we use a simpler overall menu structure.
$rules = core_component::get_plugin_list_with_file('cloudexamaccess', 'settings.php', false);
$rulesbyname = array();
foreach ($rules as $rule => $ruledir) {
    $strrulename = get_string('pluginname', 'cloudexamaccess_' . $rule);
    $rulesbyname[$strrulename] = $rule;
}
core_collator::ksort($rulesbyname);

// Create the cloudexam settings page.
if (empty($reportsbyname) && empty($rulesbyname)) {
    $pagetitle = get_string('modulename', 'cloudexam');
} else {
    $pagetitle = get_string('generalsettings', 'admin');
}
$cloudexamsettings = new admin_settingpage('modsettingcloudexam', $pagetitle, 'moodle/site:config');

if ($ADMIN->fulltree) {
    /*
     *
     * Settings for the cloudexam configuration
     *
     */
    $cloudexamsettings->add(new admin_setting_heading('cloudexamconfigsettings', 'EEP', get_string('cloudexamconfigsettings', 'cloudexam')));

    // Shared secret
    $cloudexamsettings->add(new admin_setting_configtext('cloudexam/sharedsecret',
            get_string('sharedsecret', 'cloudexam'), get_string('configsharedsecret', 'cloudexam'),
            null, PARAM_TEXT));

    // destination URL for configs
    $cloudexamsettings->add(new admin_setting_configtext('cloudexam/eepurl',
            get_string('eepurl', 'cloudexam'), get_string('configeepurl', 'cloudexam'),
            null, PARAM_RAW));



    // Introductory explanation that all the settings are defaults for the add cloudexam form.
    $cloudexamsettings->add(new admin_setting_heading('cloudexamintro', 'General', get_string('configintro', 'cloudexam')));

    // Path for json.
    $cloudexamsettings->add(new admin_setting_configfile('cloudexam/jsonpath',
            get_string('jsonpath', 'cloudexam'), get_string('configjsonpathdesc', 'cloudexam'),PARAM_PATH));
    // ID of web service user that attempts quizzes on behalf of users.
    $cloudexamsettings->add(new admin_setting_configtext('cloudexam/wsid',
            get_string('wsid', 'cloudexam'), get_string('configwsiddesc', 'cloudexam'),PARAM_INT));
    
    // Time limit.
    $cloudexamsettings->add(new admin_setting_configduration_with_advanced('cloudexam/timelimit',
            get_string('timelimit', 'cloudexam'), get_string('configtimelimitsec', 'cloudexam'),
            array('value' => '0', 'adv' => false), 60));

    // What to do with overdue attempts.
    $cloudexamsettings->add(new mod_cloudexam_admin_setting_overduehandling('cloudexam/overduehandling',
            get_string('overduehandling', 'cloudexam'), get_string('overduehandling_desc', 'cloudexam'),
            array('value' => 'autosubmit', 'adv' => false), null));

    // Grace period time.
    $cloudexamsettings->add(new admin_setting_configduration_with_advanced('cloudexam/graceperiod',
            get_string('graceperiod', 'cloudexam'), get_string('graceperiod_desc', 'cloudexam'),
            array('value' => '86400', 'adv' => false)));

    // Minimum grace period used behind the scenes.
    $cloudexamsettings->add(new admin_setting_configduration('cloudexam/graceperiodmin',
            get_string('graceperiodmin', 'cloudexam'), get_string('graceperiodmin_desc', 'cloudexam'),
            60, 1));

    // Number of attempts.
    $options = array(get_string('unlimited'));
    for ($i = 1; $i <= CLOUDEXAM_MAX_ATTEMPT_OPTION; $i++) {
        $options[$i] = $i;
    }
    $cloudexamsettings->add(new admin_setting_configselect_with_advanced('cloudexam/attempts',
            get_string('attemptsallowed', 'cloudexam'), get_string('configattemptsallowed', 'cloudexam'),
            array('value' => 0, 'adv' => false), $options));

    // Grading method.
    $cloudexamsettings->add(new mod_cloudexam_admin_setting_grademethod('cloudexam/grademethod',
            get_string('grademethod', 'cloudexam'), get_string('configgrademethod', 'cloudexam'),
            array('value' => CLOUDEXAM_GRADEHIGHEST, 'adv' => false), null));

    // Maximum grade.
    $cloudexamsettings->add(new admin_setting_configtext('cloudexam/maximumgrade',
            get_string('maximumgrade'), get_string('configmaximumgrade', 'cloudexam'), 10, PARAM_INT));

    // Questions per page.
    $perpage = array();
    $perpage[0] = get_string('never');
    $perpage[1] = get_string('aftereachquestion', 'cloudexam');
    for ($i = 2; $i <= CLOUDEXAM_MAX_QPP_OPTION; ++$i) {
        $perpage[$i] = get_string('afternquestions', 'cloudexam', $i);
    }
    $cloudexamsettings->add(new admin_setting_configselect_with_advanced('cloudexam/questionsperpage',
            get_string('newpageevery', 'cloudexam'), get_string('confignewpageevery', 'cloudexam'),
            array('value' => 1, 'adv' => false), $perpage));

    // Navigation method.
    $cloudexamsettings->add(new admin_setting_configselect_with_advanced('cloudexam/navmethod',
            get_string('navmethod', 'cloudexam'), get_string('confignavmethod', 'cloudexam'),
            array('value' => CLOUDEXAM_NAVMETHOD_FREE, 'adv' => true), cloudexam_get_navigation_options()));

    // Shuffle within questions.
    $cloudexamsettings->add(new admin_setting_configcheckbox_with_advanced('cloudexam/shuffleanswers',
            get_string('shufflewithin', 'cloudexam'), get_string('configshufflewithin', 'cloudexam'),
            array('value' => 1, 'adv' => false)));

    // Preferred behaviour.
    $cloudexamsettings->add(new admin_setting_question_behaviour('cloudexam/preferredbehaviour',
            get_string('howquestionsbehave', 'question'), get_string('howquestionsbehave_desc', 'cloudexam'),
            'deferredfeedback'));

    // Can redo completed questions.
    $cloudexamsettings->add(new admin_setting_configselect_with_advanced('cloudexam/canredoquestions',
            get_string('canredoquestions', 'cloudexam'), get_string('canredoquestions_desc', 'cloudexam'),
            array('value' => 0, 'adv' => true),
            array(0 => get_string('no'), 1 => get_string('canredoquestionsyes', 'cloudexam'))));

    // Each attempt builds on last.
    $cloudexamsettings->add(new admin_setting_configcheckbox_with_advanced('cloudexam/attemptonlast',
            get_string('eachattemptbuildsonthelast', 'cloudexam'),
            get_string('configeachattemptbuildsonthelast', 'cloudexam'),
            array('value' => 0, 'adv' => true)));

    // Review options.
    $cloudexamsettings->add(new admin_setting_heading('reviewheading',
            get_string('reviewoptionsheading', 'cloudexam'), ''));
    foreach (mod_cloudexam_admin_review_setting::fields() as $field => $name) {
        $default = mod_cloudexam_admin_review_setting::all_on();
        $forceduring = null;
        if ($field == 'attempt') {
            $forceduring = true;
        } else if ($field == 'overallfeedback') {
            $default = $default ^ mod_cloudexam_admin_review_setting::DURING;
            $forceduring = false;
        }
        $cloudexamsettings->add(new mod_cloudexam_admin_review_setting('cloudexam/review' . $field,
                $name, '', $default, $forceduring));
    }

    // Show the user's picture.
    $cloudexamsettings->add(new mod_cloudexam_admin_setting_user_image('cloudexam/showuserpicture',
            get_string('showuserpicture', 'cloudexam'), get_string('configshowuserpicture', 'cloudexam'),
            array('value' => 0, 'adv' => false), null));

    // Decimal places for overall grades.
    $options = array();
    for ($i = 0; $i <= CLOUDEXAM_MAX_DECIMAL_OPTION; $i++) {
        $options[$i] = $i;
    }
    $cloudexamsettings->add(new admin_setting_configselect_with_advanced('cloudexam/decimalpoints',
            get_string('decimalplaces', 'cloudexam'), get_string('configdecimalplaces', 'cloudexam'),
            array('value' => 2, 'adv' => false), $options));

    // Decimal places for question grades.
    $options = array(-1 => get_string('sameasoverall', 'cloudexam'));
    for ($i = 0; $i <= CLOUDEXAM_MAX_Q_DECIMAL_OPTION; $i++) {
        $options[$i] = $i;
    }
    $cloudexamsettings->add(new admin_setting_configselect_with_advanced('cloudexam/questiondecimalpoints',
            get_string('decimalplacesquestion', 'cloudexam'),
            get_string('configdecimalplacesquestion', 'cloudexam'),
            array('value' => -1, 'adv' => true), $options));

    // Show blocks during cloudexam attempts.
    $cloudexamsettings->add(new admin_setting_configcheckbox_with_advanced('cloudexam/showblocks',
            get_string('showblocks', 'cloudexam'), get_string('configshowblocks', 'cloudexam'),
            array('value' => 0, 'adv' => true)));

    // Password.
    $cloudexamsettings->add(new admin_setting_configtext_with_advanced('cloudexam/password',
            get_string('requirepassword', 'cloudexam'), get_string('configrequirepassword', 'cloudexam'),
            array('value' => '', 'adv' => false), PARAM_TEXT));

    // IP restrictions.
    $cloudexamsettings->add(new admin_setting_configtext_with_advanced('cloudexam/subnet',
            get_string('requiresubnet', 'cloudexam'), get_string('configrequiresubnet', 'cloudexam'),
            array('value' => '', 'adv' => true), PARAM_TEXT));

    // Enforced delay between attempts.
    $cloudexamsettings->add(new admin_setting_configduration_with_advanced('cloudexam/delay1',
            get_string('delay1st2nd', 'cloudexam'), get_string('configdelay1st2nd', 'cloudexam'),
            array('value' => 0, 'adv' => true), 60));
    $cloudexamsettings->add(new admin_setting_configduration_with_advanced('cloudexam/delay2',
            get_string('delaylater', 'cloudexam'), get_string('configdelaylater', 'cloudexam'),
            array('value' => 0, 'adv' => true), 60));

    // Browser security.
    $cloudexamsettings->add(new mod_cloudexam_admin_setting_browsersecurity('cloudexam/browsersecurity',
            get_string('showinsecurepopup', 'cloudexam'), get_string('configpopup', 'cloudexam'),
            array('value' => '-', 'adv' => true), null));

    $cloudexamsettings->add(new admin_setting_configtext('cloudexam/initialnumfeedbacks',
            get_string('initialnumfeedbacks', 'cloudexam'), get_string('initialnumfeedbacks_desc', 'cloudexam'),
            2, PARAM_INT, 5));

    // Allow user to specify if setting outcomes is an advanced setting.
    if (!empty($CFG->enableoutcomes)) {
        $cloudexamsettings->add(new admin_setting_configcheckbox('cloudexam/outcomes_adv',
            get_string('outcomesadvanced', 'cloudexam'), get_string('configoutcomesadvanced', 'cloudexam'),
            '0'));
    }

    // Autosave frequency.
    $cloudexamsettings->add(new admin_setting_configduration('cloudexam/autosaveperiod',
            get_string('autosaveperiod', 'cloudexam'), get_string('autosaveperiod_desc', 'cloudexam'), 60, 1));
}

// Now, depending on whether any reports have their own settings page, add
// the cloudexam setting page to the appropriate place in the tree.
if (empty($reportsbyname) && empty($rulesbyname)) {
    $ADMIN->add('modsettings', $cloudexamsettings);
} else {
    $ADMIN->add('modsettings', new admin_category('modsettingscloudexamcat',
            get_string('modulename', 'cloudexam'), $module->is_enabled() === false));
    $ADMIN->add('modsettingscloudexamcat', $cloudexamsettings);

    // Add settings pages for the cloudexam report subplugins.
    foreach ($reportsbyname as $strreportname => $report) {
        $reportname = $report;

        $settings = new admin_settingpage('modsettingscloudexamcat'.$reportname,
                $strreportname, 'moodle/site:config', $module->is_enabled() === false);
        if ($ADMIN->fulltree) {
            include($CFG->dirroot . "/mod/cloudexam/report/$reportname/settings.php");
        }
        if (!empty($settings)) {
            $ADMIN->add('modsettingscloudexamcat', $settings);
        }
    }

    // Add settings pages for the cloudexam access rule subplugins.
    foreach ($rulesbyname as $strrulename => $rule) {
        $settings = new admin_settingpage('modsettingscloudexamcat' . $rule,
                $strrulename, 'moodle/site:config', $module->is_enabled() === false);
        if ($ADMIN->fulltree) {
            include($CFG->dirroot . "/mod/cloudexam/accessrule/$rule/settings.php");
        }
        if (!empty($settings)) {
            $ADMIN->add('modsettingscloudexamcat', $settings);
        }
    }
}

$settings = null; // We do not want standard settings link.

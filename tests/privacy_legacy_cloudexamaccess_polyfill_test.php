<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Unit tests for the privacy legacy polyfill for cloudexam access rules.
 *
 * @package     mod_cloudexam
 * @category    test
 * @based on    original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright   2019 onwards Edunao SA
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/mod/cloudexam/attemptlib.php');

/**
 * Unit tests for the privacy legacy polyfill for cloudexam access rules.
 *
 * @based on    original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright   2019 onwards Edunao SA
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class core_privacy_legacy_cloudexamaccess_polyfill_test extends advanced_testcase {
    /**
     * Test that the core_cloudexamaccess\privacy\legacy_polyfill works and that the static _export_cloudexamaccess_user_data can
     * be called.
     */
    public function test_export_cloudexamaccess_user_data() {
        $cloudexam = $this->createMock(cloudexam::class);
        $user = (object) [];
        $returnvalue = (object) [];

        $mock = $this->createMock(test_privacy_legacy_cloudexamaccess_polyfill_mock_wrapper::class);
        $mock->expects($this->once())
            ->method('get_return_value')
            ->with('_export_cloudexamaccess_user_data', [$cloudexam, $user])
            ->willReturn($returnvalue);

        test_privacy_legacy_cloudexamaccess_polyfill_provider::$mock = $mock;
        $result = test_privacy_legacy_cloudexamaccess_polyfill_provider::export_cloudexamaccess_user_data($cloudexam, $user);
        $this->assertSame($returnvalue, $result);
    }

    /**
     * Test the _delete_cloudexamaccess_for_context shim.
     */
    public function test_delete_cloudexamaccess_for_context() {
        $context = context_system::instance();

        $cloudexam = $this->createMock(cloudexam::class);

        $mock = $this->createMock(test_privacy_legacy_cloudexamaccess_polyfill_mock_wrapper::class);
        $mock->expects($this->once())
            ->method('get_return_value')
            ->with('_delete_cloudexamaccess_data_for_all_users_in_context', [$cloudexam]);

        test_privacy_legacy_cloudexamaccess_polyfill_provider::$mock = $mock;
        test_privacy_legacy_cloudexamaccess_polyfill_provider::delete_cloudexamaccess_data_for_all_users_in_context($cloudexam);
    }

    /**
     * Test the _delete_cloudexamaccess_for_context shim.
     */
    public function test_delete_cloudexamaccess_for_user() {
        $context = context_system::instance();

        $cloudexam = $this->createMock(cloudexam::class);
        $user = (object) [];

        $mock = $this->createMock(test_privacy_legacy_cloudexamaccess_polyfill_mock_wrapper::class);
        $mock->expects($this->once())
            ->method('get_return_value')
            ->with('_delete_cloudexamaccess_data_for_user', [$cloudexam, $user]);

        test_privacy_legacy_cloudexamaccess_polyfill_provider::$mock = $mock;
        test_privacy_legacy_cloudexamaccess_polyfill_provider::delete_cloudexamaccess_data_for_user($cloudexam, $user);
    }
}

/**
 * Legacy polyfill test class for the cloudexamaccess_provider.
 *
 * @based on    original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright   2019 onwards Edunao SA
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class test_privacy_legacy_cloudexamaccess_polyfill_provider implements
        \core_privacy\local\metadata\provider,
        \mod_cloudexam\privacy\cloudexamaccess_provider {

    use \mod_cloudexam\privacy\legacy_cloudexamaccess_polyfill;
    use \core_privacy\local\legacy_polyfill;

    /**
     * @var test_privacy_legacy_cloudexamaccess_polyfill_provider $mock.
     */
    public static $mock = null;

    /**
     * Export all user data for the cloudexamaccess plugin.
     *
     * @param \cloudexam $cloudexam
     * @param \stdClass $user
     */
    protected static function _export_cloudexamaccess_user_data($cloudexam, $user) {
        return static::$mock->get_return_value(__FUNCTION__, func_get_args());
    }

    /**
     * Deletes all user data for the given context.
     *
     * @param \cloudexam $cloudexam
     */
    protected static function _delete_cloudexamaccess_data_for_all_users_in_context($cloudexam) {
        static::$mock->get_return_value(__FUNCTION__, func_get_args());
    }

    /**
     * Delete personal data for the given user and context.
     *
     * @param   \cloudexam           $cloudexam The cloudexam being deleted
     * @param   \stdClass       $user The user to export data for
     */
    protected static function _delete_cloudexamaccess_data_for_user($cloudexam, $user) {
        static::$mock->get_return_value(__FUNCTION__, func_get_args());
    }

    /**
     * Returns metadata about this plugin.
     *
     * @param   \core_privacy\local\metadata\collection $collection The initialised collection to add items to.
     * @return  \core_privacy\local\metadata\collection     A listing of user data stored through this system.
     */
    protected static function _get_metadata(\core_privacy\local\metadata\collection $collection) {
        return $collection;
    }
}

/**
 * Called inside the polyfill methods in the test polyfill provider, allowing us to ensure these are called with correct params.
 *
 * @based on    original work with copyright: 2018 Andrew Nicols <andrew@nicols.co.uk>
 * @copyright   2019 onwards Edunao SA
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class test_privacy_legacy_cloudexamaccess_polyfill_mock_wrapper {
    /**
     * Get the return value for the specified item.
     */
    public function get_return_value() {
    }
}

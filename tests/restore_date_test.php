<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Restore date tests.
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 2017 onwards Ankit Agarwal <ankit.agrr@gmail.com>
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . "/phpunit/classes/restore_date_testcase.php");

/**
 * Restore date tests.
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 2017 onwards Ankit Agarwal <ankit.agrr@gmail.com>
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_cloudexam_restore_date_testcase extends restore_date_testcase
{

    /**
     * Test restore dates.
     */
    public function test_restore_dates() {
        global $DB, $USER;

        // Create cloudexam data.
        $record = ['timeopen' => 100, 'timeclose' => 100, 'timemodified' => 100, 'tiemcreated' => 100, 'questionsperpage' => 0,
            'grade' => 100.0, 'sumgrades' => 2];
        list($course, $cloudexam) = $this->create_course_and_module('cloudexam', $record);

        // Create questions.
        $questiongenerator = $this->getDataGenerator()->get_plugin_generator('core_question');
        $cat = $questiongenerator->create_question_category();
        $saq = $questiongenerator->create_question('shortanswer', null, array('category' => $cat->id));
        // Add to the cloudexam.
        cloudexam_add_cloudexam_question($saq->id, $cloudexam);

        // Create an attempt.
        $timestamp = 100;
        $cloudexamobj = cloudexam::create($cloudexam->id);
        $attempt = cloudexam_create_attempt($cloudexamobj, 1, false, $timestamp, false);
        $quba = question_engine::make_questions_usage_by_activity('mod_cloudexam', $cloudexamobj->get_context());
        $quba->set_preferred_behaviour($cloudexamobj->get_cloudexam()->preferredbehaviour);
        cloudexam_start_new_attempt($cloudexamobj, $quba, $attempt, 1, $timestamp);
        cloudexam_attempt_save_started($cloudexamobj, $quba, $attempt);

        // CloudExam grade.
        $grade = new stdClass();
        $grade->cloudexam = $cloudexam->id;
        $grade->userid = $USER->id;
        $grade->grade = 8.9;
        $grade->timemodified = $timestamp;
        $grade->id = $DB->insert_record('cloudexam_grades', $grade);

        // User override.
        $override = (object)[
            'cloudexam' => $cloudexam->id,
            'groupid' => 0,
            'userid' => $USER->id,
            'sortorder' => 1,
            'timeopen' => 100,
            'timeclose' => 200
        ];
        $DB->insert_record('cloudexam_overrides', $override);

        // Set time fields to a constant for easy validation.
        $DB->set_field('cloudexam_attempts', 'timefinish', $timestamp);

        // Do backup and restore.
        $newcourseid = $this->backup_and_restore($course);
        $newcloudexam = $DB->get_record('cloudexam', ['course' => $newcourseid]);

        $this->assertFieldsNotRolledForward($cloudexam, $newcloudexam, ['timecreated', 'timemodified']);
        $props = ['timeclose', 'timeopen'];
        $this->assertFieldsRolledForward($cloudexam, $newcloudexam, $props);

        $newattempt = $DB->get_record('cloudexam_attempts', ['cloudexam' => $newcloudexam->id]);
        $newoverride = $DB->get_record('cloudexam_overrides', ['cloudexam' => $newcloudexam->id]);
        $newgrade = $DB->get_record('cloudexam_grades', ['cloudexam' => $newcloudexam->id]);

        // Attempt time checks.
        $diff = $this->get_diff();
        $this->assertEquals($timestamp, $newattempt->timemodified);
        $this->assertEquals($timestamp, $newattempt->timefinish);
        $this->assertEquals($timestamp, $newattempt->timestart);
        $this->assertEquals($timestamp + $diff, $newattempt->timecheckstate); // Should this be rolled?

        // CloudExam override time checks.
        $diff = $this->get_diff();
        $this->assertEquals($override->timeopen + $diff, $newoverride->timeopen);
        $this->assertEquals($override->timeclose + $diff, $newoverride->timeclose);

        // CloudExam grade time checks.
        $this->assertEquals($grade->timemodified, $newgrade->timemodified);
    }
}

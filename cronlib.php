<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library code used by cloudexam cron.
 *
 * @package   mod_cloudexam
 * @based on  original work with copyright: 2012 the Open University
 * @copyright 2019 onwards Edunao SA
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/locallib.php');


/**
 * This class holds all the code for automatically updating all attempts that have
 * gone over their time limit.
 *
 * @based on  original work with copyright: 2012 the Open University
 * @copyright 2019 onwards Edunao SA
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_cloudexam_overdue_attempt_updater {

    /**
     * Do the processing required.
     * @param int $timenow the time to consider as 'now' during the processing.
     * @param int $processto only process attempt with timecheckstate longer ago than this.
     * @return array with two elements, the number of attempt considered, and how many different cloudexams that was.
     */
    public function update_overdue_attempts($timenow, $processto) {
        global $DB;

        $attemptstoprocess = $this->get_list_of_overdue_attempts($processto);

        $course = null;
        $cloudexam = null;
        $cm = null;

        $count = 0;
        $cloudexamcount = 0;
        foreach ($attemptstoprocess as $attempt) {
            try {

                // If we have moved on to a different cloudexam, fetch the new data.
                if (!$cloudexam || $attempt->cloudexam != $cloudexam->id) {
                    $cloudexam = $DB->get_record('cloudexam', array('id' => $attempt->cloudexam), '*', MUST_EXIST);
                    $cm = get_coursemodule_from_instance('cloudexam', $attempt->cloudexam);
                    $cloudexamcount += 1;
                }

                // If we have moved on to a different course, fetch the new data.
                if (!$course || $course->id != $cloudexam->course) {
                    $course = $DB->get_record('course', array('id' => $cloudexam->course), '*', MUST_EXIST);
                }

                // Make a specialised version of the cloudexam settings, with the relevant overrides.
                $cloudexamforuser = clone($cloudexam);
                $cloudexamforuser->timeclose = $attempt->usertimeclose;
                $cloudexamforuser->timelimit = $attempt->usertimelimit;

                // Trigger any transitions that are required.
                $attemptobj = new cloudexam_attempt($attempt, $cloudexamforuser, $cm, $course);
                $attemptobj->handle_if_time_expired($timenow, false);
                $count += 1;

            } catch (moodle_exception $e) {
                // If an error occurs while processing one attempt, don't let that kill cron.
                mtrace("Error while processing attempt {$attempt->id} at {$attempt->cloudexam} cloudexam:");
                mtrace($e->getMessage());
                mtrace($e->getTraceAsString());
                // Close down any currently open transactions, otherwise one error
                // will stop following DB changes from being committed.
                $DB->force_transaction_rollback();
            }
        }

        $attemptstoprocess->close();
        return array($count, $cloudexamcount);
    }

    /**
     * @return moodle_recordset of cloudexam_attempts that need to be processed because time has
     *     passed. The array is sorted by courseid then cloudexamid.
     */
    public function get_list_of_overdue_attempts($processto) {
        global $DB;


        // SQL to compute timeclose and timelimit for each attempt:
        $cloudexamausersql = cloudexam_get_attempt_usertime_sql(
                "icloudexama.state IN ('inprogress', 'overdue') AND icloudexama.timecheckstate <= :iprocessto");

        // This query should have all the cloudexam_attempts columns.
        return $DB->get_recordset_sql("
         SELECT cloudexama.*,
                cloudexamauser.usertimeclose,
                cloudexamauser.usertimelimit

           FROM {cloudexam_attempts} cloudexama
           JOIN {cloudexam} cloudexam ON cloudexam.id = cloudexama.cloudexam
           JOIN ( $cloudexamausersql ) cloudexamauser ON cloudexamauser.id = cloudexama.id

          WHERE cloudexama.state IN ('inprogress', 'overdue')
            AND cloudexama.timecheckstate <= :processto
       ORDER BY cloudexam.course, cloudexama.cloudexam",

                array('processto' => $processto, 'iprocessto' => $processto));
    }
}

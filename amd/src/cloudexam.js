// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_cloudexam
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     celine <celine@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery'], function ($) {
    var cloudExam = {
        limit_qtype_qcm: function () {
            var multichoice = $("#item_qtype_multichoice").parent().parent();
            $(".alloptions .option").remove();
            $(".alloptions .moduletypetitle").last().remove();
            $(".alloptions").append(multichoice);
        },
        show_exam_status: function (data) {

            // Set the date we're counting down to
            var examtimeopen = new Date(data.date).getTime();

            // Update the count down every 1 second
            var interval = setInterval(function () {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the time difference between now and the count down date
                // We have to wait 1 more second (1000 milliseconds), time to open the exam
                var timeleft = examtimeopen - now + 1000;

                // If cloudexam is open, move user on the exam page
                if (timeleft < 0) {
                    clearInterval(interval);
                    window.location.href = data.examurl + data.id;
                }

                // Time calculations for minutes and seconds
                timeleft = Math.trunc(timeleft / 1000);
                var a = {};
                a.min = Math.trunc((timeleft % 3600) / 60);
                a.sec = Math.trunc(timeleft % 60);

                // If cloudexam will be open in more than an hour display exam opening date
                if (timeleft > 3600) {
                    document.getElementById("title").innerHTML = M.util.get_string('coutdown-scheduled-title', 'mod_cloudexam');
                    document.getElementById("countdown").innerHTML = data.date;
                } else { // If cloudexam will be open in less than an hour display countdown timer
                    document.getElementById("title").innerHTML = M.util.get_string('coutdown-title', 'mod_cloudexam');

                    // If timeleft is less than one minute display only seconds and remove minute ('0m 59s' to '59s')
                    if (timeleft < 60 && timeleft >= 0) {
                        document.getElementById("countdown").innerHTML = M.util.get_string('countdownsecondeonly', 'mod_cloudexam', a);
                    } else {
                        document.getElementById("countdown").innerHTML = M.util.get_string('countdown', 'mod_cloudexam', a);
                    }
                }
            }, 0);
        },
        init_attempt: function(){
        }
    };

    return cloudExam;
});

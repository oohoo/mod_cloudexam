<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Implementaton of the cloudexamaccess_timelimit plugin.
 *
 * @package    cloudexamaccess
 * @subpackage timelimit
 * @based on   original work with copyright: 2011 The Open University
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/accessrule/accessrulebase.php');


/**
 * A rule representing the time limit. It does not actually restrict access, but we use this
 * class to encapsulate some of the relevant code.
 *
 * @based on   original work with copyright: 2009 Tim Hunt
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cloudexamaccess_timelimit extends cloudexam_access_rule_base {

    public static function make(cloudexam $cloudexamobj, $timenow, $canignoretimelimits) {

        if (empty($cloudexamobj->get_cloudexam()->timelimit) || $canignoretimelimits) {
            return null;
        }

        return new self($cloudexamobj, $timenow);
    }

    public function description() {
        return get_string('cloudexamtimelimit', 'cloudexamaccess_timelimit',
                format_time($this->cloudexam->timelimit));
    }

    public function end_time($attempt) {
        return $attempt->timestart + $this->cloudexam->timelimit;
    }

    public function time_left_display($attempt, $timenow) {
        // If this is a teacher preview after the time limit expires, don't show the time_left
        $endtime = $this->end_time($attempt);
        if ($attempt->preview && $timenow > $endtime) {
            return false;
        }
        return $endtime - $timenow;
    }

    public function is_preflight_check_required($attemptid) {
        // Warning only required if the attempt is not already started.
        return $attemptid === null;
    }

    public function add_preflight_check_form_fields(mod_cloudexam_preflight_check_form $cloudexamform,
            MoodleQuickForm $mform, $attemptid) {
        $mform->addElement('header', 'honestycheckheader',
                get_string('confirmstartheader', 'cloudexamaccess_timelimit'));
        $mform->addElement('static', 'honestycheckmessage', '',
                get_string('confirmstart', 'cloudexamaccess_timelimit', format_time($this->cloudexam->timelimit)));
    }
}

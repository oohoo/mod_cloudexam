<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for the cloudexamaccess_timelimit plugin.
 *
 * @package    cloudexamaccess
 * @subpackage timelimit
 * @based on   original work with copyright: 2011 The Open University
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


$string['confirmstartheader'] = 'Timed cloudexam';
$string['confirmstart'] = 'The cloudexam has a time limit of {$a}. Time will count down from the moment you start your attempt and you must submit before it expires. Are you sure that you wish to start now?';
$string['pluginname'] = 'Time limit cloudexam access rule';
$string['privacy:metadata'] = 'The Time limit cloudexam access rule plugin does not store any personal data.';
$string['cloudexamtimelimit'] = 'Time limit: {$a}';

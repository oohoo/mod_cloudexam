<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Implementaton of the cloudexamaccess_ipaddress plugin.
 *
 * @package    cloudexamaccess
 * @subpackage ipaddress
 * @based on   original work with copyright: 2011 The Open University
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/accessrule/accessrulebase.php');


/**
 * A rule implementing the ipaddress check against the ->subnet setting.
 *
 * @based on   original work with copyright: 2009 Tim Hunt
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cloudexamaccess_ipaddress extends cloudexam_access_rule_base {

    public static function make(cloudexam $cloudexamobj, $timenow, $canignoretimelimits) {
        if (empty($cloudexamobj->get_cloudexam()->subnet)) {
            return null;
        }

        return new self($cloudexamobj, $timenow);
    }

    public function prevent_access() {
        if (address_in_subnet(getremoteaddr(), $this->cloudexam->subnet)) {
            return false;
        } else {
            return get_string('subnetwrong', 'cloudexamaccess_ipaddress');
        }
    }
}

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Implementaton of the cloudexamaccess_openclosedate plugin.
 *
 * @package    cloudexamaccess
 * @subpackage openclosedate
 * @based on   original work with copyright: 2011 The Open University
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/accessrule/accessrulebase.php');


/**
 * A rule enforcing open and close dates.
 *
 * @based on   original work with copyright: 2009 Tim Hunt
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cloudexamaccess_openclosedate extends cloudexam_access_rule_base {

    public static function make(cloudexam $cloudexamobj, $timenow, $canignoretimelimits) {
        // This rule is always used, even if the cloudexam has no open or close date.
        return new self($cloudexamobj, $timenow);
    }

    public function description() {
        $result = array();
        if ($this->timenow < $this->cloudexam->timeopen) {
            $result[] = get_string('cloudexamnotavailable', 'cloudexamaccess_openclosedate',
                    userdate($this->cloudexam->timeopen));
            if ($this->cloudexam->timeclose) {
                $result[] = get_string('cloudexamcloseson', 'cloudexam', userdate($this->cloudexam->timeclose));
            }

        } else if ($this->cloudexam->timeclose && $this->timenow > $this->cloudexam->timeclose) {
            $result[] = get_string('cloudexamclosed', 'cloudexam', userdate($this->cloudexam->timeclose));

        } else {
            if ($this->cloudexam->timeopen) {
                $result[] = get_string('cloudexamopenedon', 'cloudexam', userdate($this->cloudexam->timeopen));
            }
            if ($this->cloudexam->timeclose) {
                $result[] = get_string('cloudexamcloseson', 'cloudexam', userdate($this->cloudexam->timeclose));
            }
        }

        return $result;
    }

    public function prevent_access() {
        $message = get_string('notavailable', 'cloudexamaccess_openclosedate');

        if ($this->timenow < $this->cloudexam->timeopen) {
            return $message;
        }

        if (!$this->cloudexam->timeclose) {
            return false;
        }

        if ($this->timenow <= $this->cloudexam->timeclose) {
            return false;
        }

        if ($this->cloudexam->overduehandling != 'graceperiod') {
            return $message;
        }

        if ($this->timenow <= $this->cloudexam->timeclose + $this->cloudexam->graceperiod) {
            return false;
        }

        return $message;
    }

    public function is_finished($numprevattempts, $lastattempt) {
        return $this->cloudexam->timeclose && $this->timenow > $this->cloudexam->timeclose;
    }

    public function end_time($attempt) {
        if ($this->cloudexam->timeclose) {
            return $this->cloudexam->timeclose;
        }
        return false;
    }

    public function time_left_display($attempt, $timenow) {
        // If this is a teacher preview after the close date, do not show
        // the time.
        if ($attempt->preview && $timenow > $this->cloudexam->timeclose) {
            return false;
        }
        // Otherwise, return to the time left until the close date, providing that is
        // less than CLOUDEXAM_SHOW_TIME_BEFORE_DEADLINE.
        $endtime = $this->end_time($attempt);
        if ($endtime !== false && $timenow > $endtime - CLOUDEXAM_SHOW_TIME_BEFORE_DEADLINE) {
            return $endtime - $timenow;
        }
        return false;
    }
}

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for the cloudexamaccess_openclosedate plugin.
 *
 * @package    cloudexamaccess
 * @subpackage openclosedate
 * @based on   original work with copyright: 2011 The Open University
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


$string['notavailable'] = 'This cloudexam is not currently available';
$string['pluginname'] = 'Open and close date access rule';
$string['privacy:metadata'] = 'The Open and close date cloudexam access rule plugin does not store any personal data.';
$string['cloudexamnotavailable'] = 'The cloudexam will not be available until {$a}';

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_cloudexam
 * @subpackage backup-moodle2
 * @based on   original work with copyright: 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/cloudexam/backup/moodle2/restore_cloudexam_stepslib.php');


/**
 * cloudexam restore task that provides all the settings and steps to perform one
 * complete restore of the activity
 *
 * @based on   original work with copyright: 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_cloudexam_activity_task extends restore_activity_task {

    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        // No particular settings for this activity.
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        // CloudExam only has one structure step.
        $this->add_step(new restore_cloudexam_activity_structure_step('cloudexam_structure', 'cloudexam.xml'));
    }

    /**
     * Define the contents in the activity that must be
     * processed by the link decoder
     */
    public static function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('cloudexam', array('intro'), 'cloudexam');
        $contents[] = new restore_decode_content('cloudexam_feedback',
                array('feedbacktext'), 'cloudexam_feedback');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    public static function define_decode_rules() {
        $rules = array();

        $rules[] = new restore_decode_rule('CLOUDEXAMVIEWBYID',
                '/mod/cloudexam/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('CLOUDEXAMVIEWBYQ',
                '/mod/cloudexam/view.php?q=$1', 'cloudexam');
        $rules[] = new restore_decode_rule('CLOUDEXAMINDEX',
                '/mod/cloudexam/index.php?id=$1', 'course');

        return $rules;

    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * cloudexam test.log. It must return one array
     * of {@link restore_log_rule} objects
     */
    public static function define_restore_log_rules() {
        $rules = array();

        $rules[] = new restore_log_rule('cloudexam', 'add',
                'view.php?id={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'update',
                'view.php?id={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'view',
                'view.php?id={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'preview',
                'view.php?id={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'report',
                'report.php?id={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'editquestions',
                'view.php?id={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'delete attempt',
                'report.php?id={course_module}', '[oldattempt]');
        $rules[] = new restore_log_rule('cloudexam', 'edit override',
                'overrideedit.php?id={cloudexam_override}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'delete override',
                'overrides.php.php?cmid={course_module}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'addcategory',
                'view.php?id={course_module}', '{question_category}');
        $rules[] = new restore_log_rule('cloudexam', 'view summary',
                'summary.php?attempt={cloudexam_attempt}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'manualgrade',
                'comment.php?attempt={cloudexam_attempt}&question={question}', '{cloudexam}');
        $rules[] = new restore_log_rule('cloudexam', 'manualgrading',
                'report.php?mode=grading&q={cloudexam}', '{cloudexam}');
        // All the ones calling to review.php have two rules to handle both old and new urls
        // in any case they are always converted to new urls on restore.
        // TODO: In Moodle 2.x (x >= 5) kill the old rules.
        // Note we are using the 'cloudexam_attempt' mapping because that is the
        // one containing the cloudexam_attempt->ids old an new for cloudexam-attempt.
        $rules[] = new restore_log_rule('cloudexam', 'attempt',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'attempt',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        // Old an new for cloudexam-submit.
        $rules[] = new restore_log_rule('cloudexam', 'submit',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'submit',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}');
        // Old an new for cloudexam-review.
        $rules[] = new restore_log_rule('cloudexam', 'review',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'review',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}');
        // Old an new for cloudexam-start attemp.
        $rules[] = new restore_log_rule('cloudexam', 'start attempt',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'start attempt',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}');
        // Old an new for cloudexam-close attemp.
        $rules[] = new restore_log_rule('cloudexam', 'close attempt',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'close attempt',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}');
        // Old an new for cloudexam-continue attempt.
        $rules[] = new restore_log_rule('cloudexam', 'continue attempt',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, null, 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'continue attempt',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}');
        // Old an new for cloudexam-continue attemp.
        $rules[] = new restore_log_rule('cloudexam', 'continue attemp',
                'review.php?id={course_module}&attempt={cloudexam_attempt}', '{cloudexam}',
                null, 'continue attempt', 'review.php?attempt={cloudexam_attempt}');
        $rules[] = new restore_log_rule('cloudexam', 'continue attemp',
                'review.php?attempt={cloudexam_attempt}', '{cloudexam}',
                null, 'continue attempt');

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course test.log. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course test.log
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    public static function define_restore_log_rules_for_course() {
        $rules = array();

        $rules[] = new restore_log_rule('cloudexam', 'view all', 'index.php?id={course}', null);

        return $rules;
    }
}

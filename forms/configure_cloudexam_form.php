<?php


// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <celine@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class configure_cloudexam_form extends moodleform {
    public function definition() {
        $mform = $this->_form;

        $mform->addElement('editor', 'messagetostudent', get_string('messagetostudent', 'cloudexam'));
        $mform->setType('messagetostudent', PARAM_RAW);

        $mform->addElement('date_time_selector', 'startdatetime', get_string('startdatetime', 'cloudexam'));
        $mform->setType('startdatetime', PARAM_RAW);

        $mform->addElement('duration', 'duration', get_string('duration', 'cloudexam'));
        $mform->setType('duration', PARAM_RAW);

        $mform->addElement('hidden', 'cmid');
        $mform->setType('cmid', PARAM_INT);

        $this->add_action_buttons();
        $mform->disable_form_change_checker();
    }
}

<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains useful functions
 *
 * @package     mod
 * @subpackage  cloudexam/lib
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <celine@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_cloudexam;

// Required lib
require_once $CFG->dirroot . '/mod/cloudexam/attemptlib.php';
require_once $CFG->dirroot . '/mod/cloudexam/accessmanager.php';
require_once $CFG->dirroot . '/mod/cloudexam/lib.php';

/**
 * Checks if cloudexam is open
 *
 * @param int $id course module identifier
 * @return bool
 * @throws \dml_exception
 */
function is_open($cloudexamid) {
    global $DB;
    $cloudexam = $DB->get_record('cloudexam', array('id' => $cloudexamid));
    if ($cloudexam) {
        if ($cloudexam->timeopen < time() && $cloudexam->timeclose > time()) {
            return true;
        }
    }
    return false;

}

/**
 * Get the duration of exam
 *
 * @param $exam
 * @return mixed
 */
function get_exam_duration($cloudexam) {
    return $cloudexam->timeclose - $cloudexam->timeopen;
}

/**
 * @param $cmid
 * @return array|bool
 * @throws \dml_exception
 */
function get_exam_settings($cmid) {
    global $DB;

    $module = $DB->get_record('modules', array('name' => 'cloudexam'));

    $cm = $DB->get_record('course_modules', array('id' => $cmid, 'module'=> $module->id));

    if (!$cloudexam = $DB->get_record('cloudexam', array('id' => $cm->instance))) {
        return false;
    }

    if (!$cloudexam->timeopen || !$cloudexam->timeclose) {
        return false;
    }

    $cloudexamsettings = [
            'timeopen' => $cloudexam->timeopen,
            'timeclose' => $cloudexam->timeclose
    ];

    return $cloudexamsettings;
}

function check_eep_link($message = false){
    $call = new eep_call();
    $result = $call->check_eep_link();
    if(!$message){
        return isset($result->error) ? false : true;
    }

    $details = '';
    if(isset($result->errors)||$result==false){
        $details .= 'Votre Moodle n\'est pas correctement lié à EEP. Votre examen ne pourra pas être lancé correctement, veuillez contacter votre administrateur.';
        foreach($result->errors as $error){
            $details .= '<li>' . $error . '</li>';
        }
    }else{
        $details = true;
    }

    return $details;
}

function close_exam($cmid){
    global $DB;

    $module = $DB->get_record('modules', array('name' => 'cloudexam'));

    $cm = $DB->get_record('course_modules', array('id' => $cmid, 'module'=> $module->id));

    if (!$cloudexam = $DB->get_record('cloudexam', array('id' => $cm->instance))) {
        return false;
    }

    // Close exam
    if($cloudexam->timeclose > time()){
        $cloudexam->timeclose = time();
        $DB->update_record('cloudexam', $cloudexam);
    }

    $call = new eep_call();
    return $call->close_exam($cmid);
}
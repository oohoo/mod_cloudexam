<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *  Displays an iframe with the attempt form
 *
 * @package    classes
 * @subpackage pages
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <vincent.cros@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_cloudexam\eep_call;


require_once '../../../config.php';
require_once $CFG->dirroot . '/mod/cloudexam/lib/cloudexam_lib.php';
require_once $CFG->dirroot . '/mod/cloudexam/classes/eep_call.class.php';

// TODO get autorized domain correctly
$authorizeddomain = 'http://eep.dlm/';
header('Access-Control-Allow-Origin:'.$authorizeddomain);

$sitecmid = required_param('sitecmid', PARAM_INT);

$cm     = $DB->get_record('course_modules', array('id' => $sitecmid));
require_login($cm->course, false, $cm);

if(!\mod_cloudexam\is_open($cm->instance)){
    redirect($CFG->wwwroot . '/mod/cloudexam/view.php?id=' . $sitecmid);
}

$context = context_module::instance($cm->id);
$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$PAGE->set_title(get_string('attemptnumber', 'cloudexam'));
$PAGE->set_heading($COURSE->shortname);
$PAGE->set_url($CFG->wwwroot . '/mod/cloudexam/pages/attempt_cloudexam.php');

$PAGE->requires->js_call_amd('mod_cloudexam/cloudexam', 'init_attempt');

echo $OUTPUT->header();

//instantiate an eep_call sends an attempt request to the EEP platform
$eep_call = new eep_call();
$url      = $eep_call->attempt_exam($USER->id, $sitecmid);

//displays the form stored on the cluster
echo '<iframe id="attempt-frame" title="Question" width="1200" frameBorder="0" height="300" src=' . $url . '></iframe>';

echo $OUTPUT->footer();
<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     vincent <vincent.cros@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once '../../../config.php';
require_once $CFG->dirroot . '/mod/cloudexam/lib/cloudexam_lib.php';

$PAGE->requires->strings_for_js(array(
    'examstatus',
    'countdownclosed',
    'countdownopen',
    'coutdown-title',
    'countdown',
    'countdownsecondeonly',
    'coutdown-scheduled-title'
), 'mod_cloudexam');

$id = required_param('cloudexamid', PARAM_INT);

if (!$cloudexam = $DB->get_record('cloudexam', array('id' => $id))) {
    $errormsg = get_string('error-cloudexam-notexist', 'mod_cloudexam', $id);
    print_error('error', 'error', '', null, $errormsg);
}

$sql = "SELECT cm.*
       FROM {course_modules} cm
       JOIN {modules} m ON (m.name = :mname AND cm.module = m.id)
       WHERE cm.instance = :instance";

$cm = $DB->get_record_sql($sql, array('instance' => $cloudexam->id, 'mname' => 'cloudexam'));

if (!$cm) {
    $errormsg = get_string('error-cminstance-notexist', 'mod_cloudexam',
        array('instance' => $cloudexam->id, 'module' => $cloudexam->id));
    print_error('error', 'error', '', null, $errormsg);
}

$course = get_course($cm->course);
require_login($course, false, $cm);

//starting date for the exam
$datecountdown     = date('M d, Y H:i:s', $cloudexam->timeopen);
$duration          = \mod_cloudexam\get_exam_duration($cloudexam);
$messagetostudents = $cloudexam->messagetostudent;
$viewurl           = $CFG->wwwroot . '/course/view.php?id=' . $cloudexam->course;

$data = [
    'duration' => $duration,
    'date'     => $datecountdown,
    'examurl'  => $CFG->wwwroot . '/mod/cloudexam/view.php?id=',
    'id'       => $cm->id,
];

if ($cloudexam->timeopen != 0 && $cloudexam->timeopen <= time() && $cloudexam->timeclose >= time()) {
    redirect(new moodle_url('/mod/cloudexam/view.php', array('id' => $cm->id)));
} else if ($cloudexam->timeopen != 0 && $cloudexam->timeclose < time()) {
    $output = [
        'title' => get_string('examstatus', 'mod_cloudexam'),
        'countdown' => get_string('countdownclosed', 'mod_cloudexam')
    ];
} else {
    $PAGE->requires->js_call_amd('mod_cloudexam/cloudexam', 'show_exam_status', ['data' => $data]);
    $PAGE->requires->css('/mod/cloudexam/styles.css');
}

$context = context_module::instance($cm->id);
$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$PAGE->set_title(get_string('waitingcloudexam', 'cloudexam'));
$PAGE->set_heading($COURSE->shortname);
$PAGE->set_url($CFG->wwwroot . '/mod/cloudexam/pages/waiting_view.php');

echo $OUTPUT->header();

echo '<div id="messagetostudents" class="text-center">' . $messagetostudents . '</div>';
echo '<div class="text-center">';
if (isset($output)) {
    echo '<p id="title">'.$output['title'].'</p>';
    echo '<p id="countdown">'.$output['countdown'].'</p>';
} else {
    echo '<p id="title"></p>';
    echo '<p id="countdown"></p>';
}
echo '</div>';
echo '<div class="row justify-content-center">';
echo '<a id="backtocourse" class="btn btn-secondary " href="' . $viewurl . '">' . get_string('backtocourse', 'mod_cloudexam') .
     '</a>';
echo '</div>';

echo $OUTPUT->footer();

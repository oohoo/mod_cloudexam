<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 *
 * @copyright  2019 Edunao SAS (contact@edunao.com)
 * @author     nicolas <nicolas.pandraud@edunao.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot . '/mod/cloudexam/lib/cloudexam_lib.php');

// Backup library

// Current activity id
$cmid = required_param('cmid', PARAM_INT);

$site   = $CFG->wwwroot;
$eepurl = get_config('cloudexam', 'eepurl');
$secret = get_config('cloudexam', 'sharedsecret');

// Backup current activity
$archivename = create_activity_backup($cmid);
$backupname  = $archivename . '.gz';

// TODO methods for settings/content
// TODO make class to encapsulate generation zip, call to communication class, delete temp files
// TODO create function for the curl
// Start of the cURL request
$ch = curl_init();

// TODO mod_form for cloudexam settings

$fp                = curl_file_create($backupname);
$cloudexamsettings = $DB->get_record('cloudexam_exam_config', array('cmid' => $cmid));

if (!$cloudexamsettings) {
    print_error('error', 'error', '', null, 'The configuration of the exam is not done');
}
$cloudexamsettings->enddatetime = get_cloudexam_endtime($cloudexamsettings);

$post = array(
        'file'               => $fp,
        'site'               => urlencode($site),
        'cmid'               => $cmid,
        'secret'             => $secret,
        'cloudexam_settings' => json_encode($cloudexamsettings)
);

$curlurl = $eepurl . '/mod/cloudexam/pages/create_cloudexam.php';

curl_setopt($ch, CURLOPT_URL, $curlurl);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = curl_exec($ch);

// End of the cURL request
curl_close($ch);

// TODO create directory for backup
// deleting archive files of client platform in the temp/backup folder
$dir = dirname($archivename);
$di  = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
$ri  = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
foreach ($ri as $file) {
    $file->isDir() ? rmdir($file) : unlink($file);
}

// TODO change error message
// Redirect to the view
if (strpos($result, 'message')) {
    print_error('error', 'error', '', null, $result);
}
redirect(new moodle_url('/mod/cloudexam/view.php', array('id' => $cmid, 'result' => $result)));
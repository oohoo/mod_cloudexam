<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Page to edit cloudexams
 *
 * This page generally has two columns:
 * The right column lists all available questions in a chosen category and
 * allows them to be edited or more to be added. This column is only there if
 * the cloudexam does not already have student attempts
 * The left column lists all questions that have been added to the current cloudexam.
 * The lecturer can add questions from the right hand list to the cloudexam or remove them
 *
 * The script also processes a number of actions:
 * Actions affecting a cloudexam:
 * up and down  Changes the order of questions and page breaks
 * addquestion  Adds a single question to the cloudexam
 * add          Adds several selected questions to the cloudexam
 * addrandom    Adds a certain number of random questions to the cloudexam
 * repaginate   Re-paginates the cloudexam
 * delete       Removes a question from the cloudexam
 * savechanges  Saves the order and grades for questions in the cloudexam
 *
 * @package    mod_cloudexam
 * @based on   original work with copyright: 1999 onwards Martin Dougiamas and others {@link http://moodle.com}
 * @copyright  2019 onwards Edunao SA
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/cloudexam/locallib.php');
require_once($CFG->dirroot . '/mod/cloudexam/addrandomform.php');
require_once($CFG->dirroot . '/question/editlib.php');
require_once($CFG->dirroot . '/question/category_class.php');

// These params are only passed from page request to request while we stay on
// this page otherwise they would go in question_edit_setup.
$scrollpos = optional_param('scrollpos', '', PARAM_INT);

list($thispageurl, $contexts, $cmid, $cm, $cloudexam, $pagevars) =
        question_edit_setup('editq', '/mod/cloudexam/edit.php', true);

$defaultcategoryobj = question_make_default_categories($contexts->all());
$defaultcategory = $defaultcategoryobj->id . ',' . $defaultcategoryobj->contextid;

$cloudexamhasattempts = cloudexam_has_attempts($cloudexam->id);

$PAGE->set_url($thispageurl);

// Get the course object and related bits.
$course = $DB->get_record('course', array('id' => $cloudexam->course), '*', MUST_EXIST);
$cloudexamobj = new cloudexam($cloudexam, $cm, $course);
$structure = $cloudexamobj->get_structure();

// You need mod/cloudexam:manage in addition to question capabilities to access this page.
require_capability('mod/cloudexam:manage', $contexts->lowest());

// Log this visit.
$params = array(
    'courseid' => $course->id,
    'context' => $contexts->lowest(),
    'other' => array(
        'cloudexamid' => $cloudexam->id
    )
);
$event = \mod_cloudexam\event\edit_page_viewed::create($params);
$event->trigger();

// Process commands ============================================================.

// Get the list of question ids had their check-boxes ticked.
$selectedslots = array();
$params = (array) data_submitted();
foreach ($params as $key => $value) {
    if (preg_match('!^s([0-9]+)$!', $key, $matches)) {
        $selectedslots[] = $matches[1];
    }
}

$afteractionurl = new moodle_url($thispageurl);
if ($scrollpos) {
    $afteractionurl->param('scrollpos', $scrollpos);
}

if (optional_param('repaginate', false, PARAM_BOOL) && confirm_sesskey()) {
    // Re-paginate the cloudexam.
    $structure->check_can_be_edited();
    $questionsperpage = optional_param('questionsperpage', $cloudexam->questionsperpage, PARAM_INT);
    cloudexam_repaginate_questions($cloudexam->id, $questionsperpage );
    cloudexam_delete_previews($cloudexam);
    redirect($afteractionurl);
}

if (($addquestion = optional_param('addquestion', 0, PARAM_INT)) && confirm_sesskey()) {
    // Add a single question to the current cloudexam.
    $structure->check_can_be_edited();
    cloudexam_require_question_use($addquestion);
    $addonpage = optional_param('addonpage', 0, PARAM_INT);
    cloudexam_add_cloudexam_question($addquestion, $cloudexam, $addonpage);
    cloudexam_delete_previews($cloudexam);
    cloudexam_update_sumgrades($cloudexam);
    $thispageurl->param('lastchanged', $addquestion);
    redirect($afteractionurl);
}

if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
    $structure->check_can_be_edited();
    $addonpage = optional_param('addonpage', 0, PARAM_INT);
    // Add selected questions to the current cloudexam.
    $rawdata = (array) data_submitted();
    foreach ($rawdata as $key => $value) { // Parse input for question ids.
        if (preg_match('!^q([0-9]+)$!', $key, $matches)) {
            $key = $matches[1];
            cloudexam_require_question_use($key);
            cloudexam_add_cloudexam_question($key, $cloudexam, $addonpage);
        }
    }
    cloudexam_delete_previews($cloudexam);
    cloudexam_update_sumgrades($cloudexam);
    redirect($afteractionurl);
}

if ($addsectionatpage = optional_param('addsectionatpage', false, PARAM_INT)) {
    // Add a section to the cloudexam.
    $structure->check_can_be_edited();
    $structure->add_section_heading($addsectionatpage);
    cloudexam_delete_previews($cloudexam);
    redirect($afteractionurl);
}

if ((optional_param('addrandom', false, PARAM_BOOL)) && confirm_sesskey()) {
    // Add random questions to the cloudexam.
    $structure->check_can_be_edited();
    $recurse = optional_param('recurse', 0, PARAM_BOOL);
    $addonpage = optional_param('addonpage', 0, PARAM_INT);
    $categoryid = required_param('categoryid', PARAM_INT);
    $randomcount = required_param('randomcount', PARAM_INT);
    cloudexam_add_random_questions($cloudexam, $addonpage, $categoryid, $randomcount, $recurse);

    cloudexam_delete_previews($cloudexam);
    cloudexam_update_sumgrades($cloudexam);
    redirect($afteractionurl);
}

if (optional_param('savechanges', false, PARAM_BOOL) && confirm_sesskey()) {

    // If rescaling is required save the new maximum.
    $maxgrade = unformat_float(optional_param('maxgrade', '', PARAM_RAW_TRIMMED), true);
    if (is_float($maxgrade) && $maxgrade >= 0) {
        cloudexam_set_grade($maxgrade, $cloudexam);
        cloudexam_update_all_final_grades($cloudexam);
        cloudexam_update_grades($cloudexam, 0, true);
    }

    redirect($afteractionurl);
}

// Get the question bank view.
$questionbank = new mod_cloudexam\question\bank\custom_view($contexts, $thispageurl, $course, $cm, $cloudexam);
$questionbank->set_cloudexam_has_attempts($cloudexamhasattempts);
$questionbank->process_actions($thispageurl, $cm);

// End of process commands =====================================================.

$PAGE->set_pagelayout('incourse');
$PAGE->set_pagetype('mod-cloudexam-edit');

$output = $PAGE->get_renderer('mod_cloudexam', 'edit');

$PAGE->set_title(get_string('editingcloudexamx', 'cloudexam', format_string($cloudexam->name)));
$PAGE->set_heading($course->fullname);
$node = $PAGE->settingsnav->find('mod_cloudexam_edit', navigation_node::TYPE_SETTING);
if ($node) {
    $node->make_active();
}
echo $OUTPUT->header();

// Initialise the JavaScript.
$cloudexameditconfig = new stdClass();
$cloudexameditconfig->url = $thispageurl->out(true, array('qbanktool' => '0'));
$cloudexameditconfig->dialoglisteners = array();
$numberoflisteners = $DB->get_field_sql("
    SELECT COALESCE(MAX(page), 1)
      FROM {cloudexam_slots}
     WHERE cloudexamid = ?", array($cloudexam->id));

for ($pageiter = 1; $pageiter <= $numberoflisteners; $pageiter++) {
    $cloudexameditconfig->dialoglisteners[] = 'addrandomdialoglaunch_' . $pageiter;
}

$PAGE->requires->js_call_amd('mod_cloudexam/cloudexam', 'limit_qtype_qcm');
$PAGE->requires->data_for_js('cloudexam_edit_config', $cloudexameditconfig);
$PAGE->requires->js('/question/qengine.js');

// Questions wrapper start.
echo html_writer::start_tag('div', array('class' => 'mod-cloudexam-edit-content'));

echo $output->edit_page($cloudexamobj, $structure, $contexts, $thispageurl, $pagevars);

// Questions wrapper end.
echo html_writer::end_tag('div');

echo $OUTPUT->footer();

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Rest endpoint for ajax editing for paging operations on the cloudexam structure.
 *
 * @package   mod_cloudexam
 * @based on  original work with copyright: 2014 The Open University
 * @copyright 2019 onwards Edunao SA
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/cloudexam/locallib.php');

$cloudexamid = required_param('cloudexamid', PARAM_INT);
$slotnumber = required_param('slot', PARAM_INT);
$repagtype = required_param('repag', PARAM_INT);

require_sesskey();
$cloudexamobj = cloudexam::create($cloudexamid);
require_login($cloudexamobj->get_course(), false, $cloudexamobj->get_cm());
require_capability('mod/cloudexam:manage', $cloudexamobj->get_context());
if (cloudexam_has_attempts($cloudexamid)) {
    $reportlink = cloudexam_attempt_summary_link_to_reports($cloudexamobj->get_cloudexam(),
                    $cloudexamobj->get_cm(), $cloudexamobj->get_context());
    throw new \moodle_exception('cannoteditafterattempts', 'cloudexam',
            new moodle_url('/mod/cloudexam/edit.php', array('cmid' => $cloudexamobj->get_cmid())), $reportlink);
}

$slotnumber++;
$repage = new \mod_cloudexam\repaginate($cloudexamid);
$repage->repaginate_slots($slotnumber, $repagtype);

$structure = $cloudexamobj->get_structure();
$slots = $structure->refresh_page_numbers_and_update_db();

redirect(new moodle_url('edit.php', array('cmid' => $cloudexamobj->get_cmid())));
